#ifndef MISAKASCENELAYERIMPL_HPP
#define MISAKASCENELAYERIMPL_HPP

#include "MisakaImageSourceImpl.hpp"
#include <KawaiiRenderer/Renderpass/KawaiiSceneLayerImpl.hpp>

class MISAKARENDERER_SHARED_EXPORT MisakaSceneLayerImpl : public MisakaImageSourceImpl, public KawaiiSceneLayerImpl
{
public:
  MisakaSceneLayerImpl(KawaiiSceneLayer *model);
  ~MisakaSceneLayerImpl();

  // KawaiiImageSourceImpl interface
protected:
  void drawQuad() override final;
};

#endif // MISAKASCENELAYERIMPL_HPP
