#ifndef MISAKAIMAGESOURCEIMPL_HPP
#define MISAKAIMAGESOURCEIMPL_HPP

#include "../MisakaRenderer_global.hpp"
#include <KawaiiRenderer/Renderpass/KawaiiImageSourceImpl.hpp>

class MISAKARENDERER_SHARED_EXPORT MisakaImageSourceImpl : public virtual KawaiiImageSourceImpl
{
  MisakaImageSourceImpl(const MisakaImageSourceImpl&) = delete;
  MisakaImageSourceImpl& operator=(const MisakaImageSourceImpl&) = delete;
public:
  MisakaImageSourceImpl(KawaiiImageSource *model);
  ~MisakaImageSourceImpl();

  // KawaiiImageSourceImpl interface
protected:
  void createCache() override final;
  void drawCache() override final;
  void updateCache() override final;
  void deleteCache() override final;
};

#endif // MISAKAIMAGESOURCEIMPL_HPP
