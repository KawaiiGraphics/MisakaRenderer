#include "MisakaTexLayerImpl.hpp"
#include "MisakaRootImpl.hpp"
#include "Textures/MisakaTextureHandle.hpp"
#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>

MisakaTexLayerImpl::MisakaTexLayerImpl(KawaiiTexLayer *model):
  KawaiiImageSourceImpl(model),
  MisakaImageSourceImpl(model),
  attachedTexHandle(nullptr)
{
  connect(model, &sib_utils::TreeNode::parentUpdated, this, [this] { setRoot(KawaiiRoot::getRoot(getModel())); });
}

MisakaTexLayerImpl::~MisakaTexLayerImpl()
{
}

void MisakaTexLayerImpl::directDraw()
{
  auto *modelObj = static_cast<KawaiiTexLayer*>(getModel());
  auto *tex = modelObj->getTexture();
  if(tex)
    {
      auto *texImpl = static_cast<KawaiiTextureImpl*>(tex->getRendererImpl());
      auto *texHandle = texImpl?
            static_cast<MisakaTextureHandle*>(texImpl->getHandle()):
            nullptr;

      if(!texHandle) return;

      float aspectRatioCorrection = 1.0;
      if(modelObj->isKeepAspectRatio())
        aspectRatioCorrection = static_cast<double>(modelObj->getTexture()->getResolution().at(1)) * static_cast<double>(getModel()->getRenderableSize().x)
            / (static_cast<double>(modelObj->getTexture()->getResolution().at(0)) * static_cast<double>(getModel()->getRenderableSize().y));

      auto &shader = root->getArrayedOverlayShaders(getModelLayersCount());
      root->gl().glEnable(GL_CULL_FACE);
      root->gl().glCullFace(GL_BACK);
      shader.bind();
      shader.setUniformValue(0, aspectRatioCorrection);
      root->ARB_bindless_texture().glProgramUniformHandleui64(shader.programId(), 1, texHandle->getTexHandle());
      root->drawOverlayQuad();
    }
}

void MisakaTexLayerImpl::drawQuad()
{ }

void MisakaTexLayerImpl::setRoot(KawaiiRoot *newRoot)
{
  auto *newRootImpl = newRoot?
        static_cast<MisakaRootImpl*>(newRoot->getRendererImpl()):
        nullptr;
  if(newRootImpl != root)
    {
      root = newRootImpl;
    }
}

uint32_t MisakaTexLayerImpl::getModelLayersCount() const
{
  auto *tex = static_cast<KawaiiTexLayer*>(getModel())->getTexture();
  if(!tex)
    return 0;
  else
    return tex->getResolution().at(2);
}

void MisakaTexLayerImpl::initConnections()
{
  setRoot(KawaiiRoot::getRoot(getModel()));
}
