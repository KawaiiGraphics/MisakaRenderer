#ifndef MISAKAGLPAINTEDLAYERIMPL_HPP
#define MISAKAGLPAINTEDLAYERIMPL_HPP

#include <KawaiiRenderer/Renderpass/KawaiiGlPaintedLayerImpl.hpp>
#include "MisakaImageSourceImpl.hpp"

class MisakaGlPaintedLayerImpl : public MisakaImageSourceImpl, public KawaiiGlPaintedLayerImpl
{
public:
  MisakaGlPaintedLayerImpl(KawaiiGlPaintedLayer *model);
  ~MisakaGlPaintedLayerImpl() = default;

  // KawaiiImageSourceImpl interface
protected:
  void drawQuad() override final;

  // KawaiiGlPaintedLayerImpl interface
protected:
  void refreshPixmaps() override final;



  // IMPLEMENT
private:
  QOpenGLContext *getGlCtx();
};

#endif // MISAKAGLPAINTEDLAYERIMPL_HPP
