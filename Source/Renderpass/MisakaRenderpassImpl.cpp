#include "MisakaRenderpassImpl.hpp"
#include "MisakaRootImpl.hpp"

#include <QPainter>
#include <array>

MisakaRenderpassImpl::MisakaRenderpassImpl(KawaiiRenderpass *renderpass):
  KawaiiRenderpassImpl(renderpass),
  root(nullptr),
  activeColorAttachments(0)
{
  onModelParentChanged();
  connect(renderpass, &sib_utils::TreeNode::parentUpdated, this, &MisakaRenderpassImpl::onModelParentChanged);
  setRenderingCacheble(false);
  renderpass->setReadGbufFFunc(std::bind(&MisakaRenderpassImpl::readGbufF, this, std::placeholders::_1, std::placeholders::_2));
  renderpass->setReadGbufUFunc(std::bind(&MisakaRenderpassImpl::readGbufU, this, std::placeholders::_1, std::placeholders::_2));
  renderpass->setReadGbufIFunc(std::bind(&MisakaRenderpassImpl::readGbufI, this, std::placeholders::_1, std::placeholders::_2));
}

MisakaRenderpassImpl::~MisakaRenderpassImpl()
{
  preDestruct();
}

void MisakaRenderpassImpl::setExternalGbuf(size_t index, GLuint glTextureId, GLint layer)
{
  if(externalGBufs.empty())
    externalGBufs.resize(getModel()->gBufCount(), {});
  if(externalGBufs[index].glTextureId != glTextureId
     || externalGBufs[index].layer != layer)
    {
      externalGBufs[index].glTextureId = glTextureId;
      externalGBufs[index].layer = layer;
      preDestruct();
    }
}

void MisakaRenderpassImpl::unuseExternalGbufs()
{
  if(!externalGBufs.empty())
    {
      externalGBufs.clear();
      preDestruct();
    }
}

void MisakaRenderpassImpl::blitSfcColor(GLint dstX, GLint dstY, GLint dstWidth, GLint dstHeight)
{
  if(getModel()->gBufCount() == 0) return;

  attachTexture(GL_COLOR_ATTACHMENT0, getGBufDescr(0));
  root->gl().glBlitNamedFramebuffer(glFboId, QOpenGLContext::currentContext()->defaultFramebufferObject(),
                                    0,0, getModel()->getRenderableSize().x, getModel()->getRenderableSize().y,
                                    dstX, dstY, dstX+dstWidth, dstY+dstHeight,
                                    GL_COLOR_BUFFER_BIT, GL_NEAREST);
}

void MisakaRenderpassImpl::create()
{
  Q_ASSERT(internalGBufs.empty());
  internalGBufs.resize(getModel()->gBufCount(), 0);
  root->gl().glCreateFramebuffers(1, &glFboId);
  for(size_t i = 0; i < internalGBufs.size(); ++i)
    if(externalGBufs.empty() || !externalGBufs[i].glTextureId)
      {
        root->gl().glCreateTextures(GL_TEXTURE_2D, 1, &internalGBufs[i]);
        root->gl().glTextureStorage2D(internalGBufs[i], 1, getGBufInternalFmt(i), getModel()->getRenderableSize().x, getModel()->getRenderableSize().y);
      }
}

void MisakaRenderpassImpl::destroy()
{
  if(!internalGBufs.empty())
    {
      for(GLuint texId: internalGBufs)
        if(texId)
          root->gl().glDeleteTextures(1, &texId);
      internalGBufs.clear();
    }
  if(glFboId)
    {
      root->gl().glDeleteFramebuffers(1, &glFboId);
      glFboId = 0;
    }
}

bool MisakaRenderpassImpl::start()
{
  if(getModel()->subpassCount() && getModel()->isRedrawNeeded())
    {
      root->gl().glBindFramebuffer(GL_DRAW_FRAMEBUFFER, glFboId);
      const auto &sz = getModel()->getRenderableSize();
      root->gl().glViewport(0,0, sz.x, sz.y);

      for(size_t i = 0; i < getModel()->gBufCount(); ++i)
        if(getModel()->getGBuf(i).needClear)
          clearGBuf(i);

      activateSubpass(0);
      return true;
    } else
    return false;
}

void MisakaRenderpassImpl::nextSubpass()
{
  activateSubpass(getCurrentSubpass());
}

void MisakaRenderpassImpl::finish()
{
  root->clearBindedGBufs();
  getModel()->markRedrawn();
  root->finalize(QSize(getModel()->getRenderableSize().x, getModel()->getRenderableSize().y));
}

void MisakaRenderpassImpl::drawCache()
{
  Q_UNREACHABLE();
}

namespace {
  template<typename T> GLenum trTypename();

  template<> GLenum trTypename<float>()
  { return GL_FLOAT; }

  template<> GLenum trTypename<int32_t>()
  { return GL_INT; }

  template<> GLenum trTypename<uint32_t>()
  { return GL_UNSIGNED_INT; }

  template<typename T>
  void readGbuf(QOpenGLFunctions_4_5_Core &gl, const MisakaRenderpassImpl::ExternalGBufDescr &gbuf, GLuint fbo, GLenum attachment, GLenum fmt, const QRect &reg, std::vector<T> &result, int height)
  {
    // todo: test and fix

    gl.glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
    if(gbuf.layer >= 0)
      gl.glFramebufferTextureLayer(GL_READ_FRAMEBUFFER, attachment, gbuf.glTextureId, 0, gbuf.layer);
    else
      gl.glFramebufferTexture(GL_READ_FRAMEBUFFER, attachment, gbuf.glTextureId, 0);

    if(attachment == GL_COLOR_ATTACHMENT0)
      gl.glReadBuffer(GL_COLOR_ATTACHMENT0);
    gl.glReadnPixels(reg.x(), height-reg.y(), reg.width(), reg.height(), fmt, trTypename<T>(), sizeof(T)*result.size(), result.data());
    if(fmt == GL_DEPTH_COMPONENT)
      {
        for(T &i: result)
          {
            i *= 2.0;
            i -= 1.0;
          }
      }
  }
}

std::vector<float> MisakaRenderpassImpl::readGbufF(uint32_t gbuf, const QRect &reg)
{
  std::vector<float> result(reg.width()*reg.height()*getGBufReadFmtComponents(gbuf));
  if(Q_LIKELY(root) && glFboId!=0)
    readGbuf(root->gl(), getGBufDescr(gbuf), glFboId, getGBufReadAttachment(gbuf), getGBufReadFmt(gbuf), reg, result, getModel()->getRenderableSize().y);
  return result;
}

std::vector<int32_t> MisakaRenderpassImpl::readGbufI(uint32_t gbuf, const QRect &reg)
{
  std::vector<int32_t> result(reg.width()*reg.height()*getGBufReadFmtComponents(gbuf));
  if(Q_LIKELY(root) && glFboId!=0)
    readGbuf(root->gl(), getGBufDescr(gbuf), glFboId, getGBufReadAttachment(gbuf), getGBufReadFmt(gbuf), reg, result, getModel()->getRenderableSize().y);
  return result;
}

std::vector<uint32_t> MisakaRenderpassImpl::readGbufU(uint32_t gbuf, const QRect &reg)
{
  std::vector<uint32_t> result(reg.width()*reg.height()*getGBufReadFmtComponents(gbuf));
  if(Q_LIKELY(root) && glFboId!=0)
    readGbuf(root->gl(), getGBufDescr(gbuf), glFboId, getGBufReadAttachment(gbuf), getGBufReadFmt(gbuf), reg, result, getModel()->getRenderableSize().y);
  return result;
}

void MisakaRenderpassImpl::setRoot(MisakaRootImpl *newRoot)
{
  if(newRoot != root)
    {
      if(root)
        preDestruct();
      root = newRoot;
    }
}

void MisakaRenderpassImpl::onModelParentChanged()
{
  KawaiiRoot *r = KawaiiRoot::getRoot(getModel());
  if(r)
    setRoot(static_cast<MisakaRootImpl*>(r->getRendererImpl()));
  else
    setRoot(nullptr);
}

GLenum MisakaRenderpassImpl::getGBufInternalFmt(size_t gbufIndex)
{
  switch(getModel()->getGBuf(gbufIndex).format)
    {
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm:
      return GL_RGBA8;
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Snorm:
      return GL_RGBA8_SNORM;
    case KawaiiRenderpass::InternalImgFormat::RGBA16_F:
      return GL_RGBA16F;
    case KawaiiRenderpass::InternalImgFormat::RGBA16_UInt:
      return GL_RGBA16UI;
    case KawaiiRenderpass::InternalImgFormat::RGBA16_SInt:
      return GL_RGBA16I;
    case KawaiiRenderpass::InternalImgFormat::RGBA32_F:
      return GL_RGBA32F;
    case KawaiiRenderpass::InternalImgFormat::RGBA32_UInt:
      return GL_RGBA32UI;
    case KawaiiRenderpass::InternalImgFormat::RGBA32_SInt:
      return GL_RGBA32I;
    case KawaiiRenderpass::InternalImgFormat::RG8_Unorm:
      return GL_RG8;
    case KawaiiRenderpass::InternalImgFormat::RG8_Snorm:
      return GL_RG8_SNORM;
    case KawaiiRenderpass::InternalImgFormat::RG16_F:
      return GL_RG16F;
    case KawaiiRenderpass::InternalImgFormat::RG16_UInt:
      return GL_RG16UI;
    case KawaiiRenderpass::InternalImgFormat::RG16_SInt:
      return GL_RG16I;
    case KawaiiRenderpass::InternalImgFormat::RG32_F:
      return GL_RG32F;
    case KawaiiRenderpass::InternalImgFormat::RG32_UInt:
      return GL_RG32UI;
    case KawaiiRenderpass::InternalImgFormat::RG32_SInt:
      return GL_RG32I;
    case KawaiiRenderpass::InternalImgFormat::R8_Unorm:
      return GL_R8;
    case KawaiiRenderpass::InternalImgFormat::R8_Snorm:
      return GL_R8_SNORM;
    case KawaiiRenderpass::InternalImgFormat::R16_F:
      return GL_R16F;
    case KawaiiRenderpass::InternalImgFormat::R16_UInt:
      return GL_R16UI;
    case KawaiiRenderpass::InternalImgFormat::R16_SInt:
      return GL_R16I;
    case KawaiiRenderpass::InternalImgFormat::R32_F:
      return GL_R32F;
    case KawaiiRenderpass::InternalImgFormat::R32_UInt:
      return GL_R32UI;
    case KawaiiRenderpass::InternalImgFormat::R32_SInt:
      return GL_R32I;

    case KawaiiRenderpass::InternalImgFormat::Depth32:
      return GL_DEPTH_COMPONENT32F;
    case KawaiiRenderpass::InternalImgFormat::Depth24:
      return GL_DEPTH_COMPONENT24;
    case KawaiiRenderpass::InternalImgFormat::Depth16:
      return GL_DEPTH_COMPONENT16;

    case KawaiiRenderpass::InternalImgFormat::Depth32Stencil8:
      return GL_DEPTH32F_STENCIL8;
    case KawaiiRenderpass::InternalImgFormat::Depth24Stencil8:
      return GL_DEPTH24_STENCIL8;
    case KawaiiRenderpass::InternalImgFormat::Depth16Stencil8:
      return GL_DEPTH24_STENCIL8;

    case KawaiiRenderpass::InternalImgFormat::Stencil8:
      return GL_STENCIL_INDEX8;
    }
  Q_UNREACHABLE();
}

namespace {
  GLenum trDepthFunc(KawaiiImageSource::DepthFunc func)
  {
    switch(func)
      {
      case KawaiiImageSource::DepthFunc::Never:
        return GL_NEVER;
      case KawaiiImageSource::DepthFunc::Less:
        return GL_LESS;
      case KawaiiImageSource::DepthFunc::LessEqual:
        return GL_LEQUAL;
      case KawaiiImageSource::DepthFunc::Equal:
        return GL_EQUAL;
      case KawaiiImageSource::DepthFunc::Greater:
        return GL_GREATER;
      case KawaiiImageSource::DepthFunc::GreaterEqual:
        return GL_GEQUAL;
      case KawaiiImageSource::DepthFunc::Always:
        return GL_ALWAYS;
      }
    Q_UNREACHABLE();
  }

  GLenum trBlendFactor(KawaiiImageSource::BlendFactor factor)
  {
    switch(factor)
      {
      case KawaiiImageSource::BlendFactor::FactorZero:
        return GL_ZERO;
      case KawaiiImageSource::BlendFactor::FactorOne:
        return GL_ONE;
      case KawaiiImageSource::BlendFactor::FactorSrcAlpha:
        return GL_SRC_ALPHA;
      case KawaiiImageSource::BlendFactor::FactorOneMinusSrcAlpha:
        return GL_ONE_MINUS_SRC_ALPHA;
      case KawaiiImageSource::BlendFactor::FactorDstAlpha:
        return GL_DST_ALPHA;
      case KawaiiImageSource::BlendFactor::FactorOneMinusDstAlpha:
        return GL_ONE_MINUS_DST_ALPHA;
      }
    Q_UNREACHABLE();
  }
}

void MisakaRenderpassImpl::activateSubpass(size_t subpassIndex)
{
  const auto &subpass = getModel()->getSubpass(subpassIndex);
  if(!subpass.imgSrc) return;

  root->gl().glTextureBarrier();
  if(subpass.depthGBuf)
    {
      attachTexture(GL_DEPTH_ATTACHMENT, getGBufDescr(*subpass.depthGBuf));
      root->glEnable(GL_DEPTH_TEST, subpass.imgSrc->getDepthTest());
      root->gl().glDepthMask(subpass.imgSrc->getDepthWrite()? GL_TRUE: GL_FALSE);
      root->gl().glDepthFunc(trDepthFunc(subpass.imgSrc->getDepthTestFunc()));
    } else
    {
      root->gl().glNamedFramebufferTexture(glFboId, GL_DEPTH_ATTACHMENT, 0, 0);
      root->gl().glDisable(GL_DEPTH_TEST);
      root->gl().glDepthMask(GL_FALSE);
      root->gl().glDepthFunc(GL_ALWAYS);
    }

  std::vector<GLenum> drawBuffers(subpass.outputGBufs.size());
  for(size_t i = 0; i < subpass.outputGBufs.size(); ++i)
    {
      attachTexture(GL_COLOR_ATTACHMENT0+i, getGBufDescr(subpass.outputGBufs[i]));
      drawBuffers[i] = GL_COLOR_ATTACHMENT0+i;
    }
  root->gl().glDrawBuffers(drawBuffers.size(), drawBuffers.data());

  for(size_t i = subpass.outputGBufs.size(); i < activeColorAttachments; ++i)
    attachTexture(GL_COLOR_ATTACHMENT0+i, { .glTextureId = 0, .layer = -1 });
  activeColorAttachments = subpass.outputGBufs.size();

  // todo: implement layered input, sampled and storage gbufs
  std::vector<GLuint> inputGbufTextures;
  for(const auto &i: subpass.inputGBufs)
    inputGbufTextures.push_back(getGBufDescr(i).glTextureId);
  root->setBindedInputGbufs(inputGbufTextures);

  std::vector<GLuint> sampledGbufTextures;
  for(const auto &i: subpass.sampledGBufs)
    sampledGbufTextures.push_back(getGBufDescr(i).glTextureId);
  root->setBindedSampledGbufs(sampledGbufTextures);

  std::vector<GLuint> storageGbufTetures;
  for(const auto &i: subpass.storageGBufs)
    storageGbufTetures.push_back(getGBufDescr(i).glTextureId);
  root->setBindedStorageGbufs(storageGbufTetures);

  root->glEnable(GL_BLEND, subpass.imgSrc->getBlendEnabled());
  if(subpass.imgSrc->getBlendEnabled())
    {
      const GLenum srcRgb = trBlendFactor(subpass.imgSrc->getSrcColorFactor()),
          srcAlpha = trBlendFactor(subpass.imgSrc->getSrcAlphaFactor()),
          dstRgb = trBlendFactor(subpass.imgSrc->getDstColorFactor()),
          dstAlpha = trBlendFactor(subpass.imgSrc->getDstAlphaFactor());
      if(srcRgb == srcAlpha && dstRgb == dstAlpha)
        root->gl().glBlendFunc(srcRgb, dstRgb);
      else
        root->gl().glBlendFuncSeparate(srcRgb, dstRgb, srcAlpha, dstAlpha);
    }
}

MisakaRenderpassImpl::ExternalGBufDescr MisakaRenderpassImpl::getGBufDescr(size_t gbufIndex) const
{
  if(!externalGBufs.empty() && externalGBufs[gbufIndex].glTextureId)
    return externalGBufs[gbufIndex];
  else
    return ExternalGBufDescr { .glTextureId = internalGBufs[gbufIndex], .layer = -1 };
}

GLenum MisakaRenderpassImpl::getGBufReadFmt(size_t gbufIndex) const
{
  switch(getModel()->getGBuf(gbufIndex).format)
    {
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_F:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_UInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_SInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_F:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_UInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_SInt:
    case KawaiiRenderpass::InternalImgFormat::RG8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::RG8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::RG16_F:
    case KawaiiRenderpass::InternalImgFormat::RG16_UInt:
    case KawaiiRenderpass::InternalImgFormat::RG16_SInt:
    case KawaiiRenderpass::InternalImgFormat::RG32_F:
    case KawaiiRenderpass::InternalImgFormat::RG32_UInt:
    case KawaiiRenderpass::InternalImgFormat::RG32_SInt:
    case KawaiiRenderpass::InternalImgFormat::R8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::R8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::R16_F:
    case KawaiiRenderpass::InternalImgFormat::R16_UInt:
    case KawaiiRenderpass::InternalImgFormat::R16_SInt:
    case KawaiiRenderpass::InternalImgFormat::R32_F:
    case KawaiiRenderpass::InternalImgFormat::R32_UInt:
    case KawaiiRenderpass::InternalImgFormat::R32_SInt:
      return GL_RGBA;

    case KawaiiRenderpass::InternalImgFormat::Depth32:
    case KawaiiRenderpass::InternalImgFormat::Depth24:
    case KawaiiRenderpass::InternalImgFormat::Depth16:
      return GL_DEPTH_COMPONENT;

    case KawaiiRenderpass::InternalImgFormat::Depth32Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Depth24Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Depth16Stencil8:
      return GL_DEPTH_STENCIL;

    case KawaiiRenderpass::InternalImgFormat::Stencil8:
      return GL_STENCIL_INDEX;
    }
  Q_UNREACHABLE();
}

uint64_t MisakaRenderpassImpl::getGBufReadFmtComponents(size_t gbufIndex) const
{
  switch(getModel()->getGBuf(gbufIndex).format)
    {
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_F:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_UInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_SInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_F:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_UInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_SInt:
    case KawaiiRenderpass::InternalImgFormat::RG8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::RG8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::RG16_F:
    case KawaiiRenderpass::InternalImgFormat::RG16_UInt:
    case KawaiiRenderpass::InternalImgFormat::RG16_SInt:
    case KawaiiRenderpass::InternalImgFormat::RG32_F:
    case KawaiiRenderpass::InternalImgFormat::RG32_UInt:
    case KawaiiRenderpass::InternalImgFormat::RG32_SInt:
    case KawaiiRenderpass::InternalImgFormat::R8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::R8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::R16_F:
    case KawaiiRenderpass::InternalImgFormat::R16_UInt:
    case KawaiiRenderpass::InternalImgFormat::R16_SInt:
    case KawaiiRenderpass::InternalImgFormat::R32_F:
    case KawaiiRenderpass::InternalImgFormat::R32_UInt:
    case KawaiiRenderpass::InternalImgFormat::R32_SInt:
      return 4;

    case KawaiiRenderpass::InternalImgFormat::Depth32:
    case KawaiiRenderpass::InternalImgFormat::Depth24:
    case KawaiiRenderpass::InternalImgFormat::Depth16:

    case KawaiiRenderpass::InternalImgFormat::Depth32Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Depth24Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Depth16Stencil8:

    case KawaiiRenderpass::InternalImgFormat::Stencil8:
      return 1;
    }
  Q_UNREACHABLE();
}

GLenum MisakaRenderpassImpl::getGBufReadAttachment(size_t gbufIndex) const
{
  switch(getModel()->getGBuf(gbufIndex).format)
    {
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_F:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_UInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_SInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_F:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_UInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_SInt:
    case KawaiiRenderpass::InternalImgFormat::RG8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::RG8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::RG16_F:
    case KawaiiRenderpass::InternalImgFormat::RG16_UInt:
    case KawaiiRenderpass::InternalImgFormat::RG16_SInt:
    case KawaiiRenderpass::InternalImgFormat::RG32_F:
    case KawaiiRenderpass::InternalImgFormat::RG32_UInt:
    case KawaiiRenderpass::InternalImgFormat::RG32_SInt:
    case KawaiiRenderpass::InternalImgFormat::R8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::R8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::R16_F:
    case KawaiiRenderpass::InternalImgFormat::R16_UInt:
    case KawaiiRenderpass::InternalImgFormat::R16_SInt:
    case KawaiiRenderpass::InternalImgFormat::R32_F:
    case KawaiiRenderpass::InternalImgFormat::R32_UInt:
    case KawaiiRenderpass::InternalImgFormat::R32_SInt:
      return GL_COLOR_ATTACHMENT0;

    case KawaiiRenderpass::InternalImgFormat::Depth32:
    case KawaiiRenderpass::InternalImgFormat::Depth24:
    case KawaiiRenderpass::InternalImgFormat::Depth16:
      return GL_DEPTH_ATTACHMENT;

    case KawaiiRenderpass::InternalImgFormat::Depth32Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Depth24Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Depth16Stencil8:
      return GL_DEPTH_STENCIL_ATTACHMENT;

    case KawaiiRenderpass::InternalImgFormat::Stencil8:
      return GL_STENCIL_ATTACHMENT;
    }
  Q_UNREACHABLE();
}

void MisakaRenderpassImpl::attachTexture(GLenum attachement, const ExternalGBufDescr &gbufDescr)
{
  if(gbufDescr.layer >= 0)
    root->gl().glNamedFramebufferTextureLayer(glFboId, attachement, gbufDescr.glTextureId, 0, gbufDescr.layer);
  else
    root->gl().glNamedFramebufferTexture(glFboId, attachement, gbufDescr.glTextureId, 0);
}

void MisakaRenderpassImpl::clearGBuf(size_t gbufIndex)
{
  static const constexpr std::array<float, 4> clearColor = {0,0,0,0};
  static const constexpr float clearDepth = 1.0;
  switch(getModel()->getGBuf(gbufIndex).format)
    {
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_F:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_UInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_SInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_F:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_UInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_SInt:
    case KawaiiRenderpass::InternalImgFormat::RG8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::RG8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::RG16_F:
    case KawaiiRenderpass::InternalImgFormat::RG16_UInt:
    case KawaiiRenderpass::InternalImgFormat::RG16_SInt:
    case KawaiiRenderpass::InternalImgFormat::RG32_F:
    case KawaiiRenderpass::InternalImgFormat::RG32_UInt:
    case KawaiiRenderpass::InternalImgFormat::RG32_SInt:
    case KawaiiRenderpass::InternalImgFormat::R8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::R8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::R16_F:
    case KawaiiRenderpass::InternalImgFormat::R16_UInt:
    case KawaiiRenderpass::InternalImgFormat::R16_SInt:
    case KawaiiRenderpass::InternalImgFormat::R32_F:
    case KawaiiRenderpass::InternalImgFormat::R32_UInt:
    case KawaiiRenderpass::InternalImgFormat::R32_SInt:
      attachTexture(GL_COLOR_ATTACHMENT0, getGBufDescr(gbufIndex));
      root->gl().glClearNamedFramebufferfv(glFboId, GL_COLOR, 0, clearColor.data());
      break;

    case KawaiiRenderpass::InternalImgFormat::Depth32:
    case KawaiiRenderpass::InternalImgFormat::Depth24:
    case KawaiiRenderpass::InternalImgFormat::Depth16:
      attachTexture(GL_DEPTH_ATTACHMENT, getGBufDescr(gbufIndex));
      root->gl().glClearNamedFramebufferfv(glFboId, GL_DEPTH, 0, &clearDepth);
      break;

    case KawaiiRenderpass::InternalImgFormat::Depth32Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Depth24Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Depth16Stencil8:
      attachTexture(GL_DEPTH_STENCIL_ATTACHMENT, getGBufDescr(gbufIndex));
      root->gl().glClearNamedFramebufferfv(glFboId, GL_DEPTH, 0, &clearDepth);
      root->gl().glClearNamedFramebufferfv(glFboId, GL_STENCIL, 0, &clearDepth);
      break;

    case KawaiiRenderpass::InternalImgFormat::Stencil8:
      attachTexture(GL_STENCIL_ATTACHMENT, getGBufDescr(gbufIndex));
      root->gl().glClearNamedFramebufferfv(glFboId, GL_STENCIL, 0, &clearDepth);
      break;

    default:
      Q_UNREACHABLE();
    }
}
