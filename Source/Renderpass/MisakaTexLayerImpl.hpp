#ifndef MISAKATEXLAYERIMPL_HPP
#define MISAKATEXLAYERIMPL_HPP

#include "MisakaImageSourceImpl.hpp"
#include "Textures/MisakaTextureHandle.hpp"

#include <Kawaii3D/Renderpass/KawaiiTexLayer.hpp>
#include <qopengl.h>

class MisakaRootImpl;
class KawaiiRoot;
class MISAKARENDERER_SHARED_EXPORT MisakaTexLayerImpl : public MisakaImageSourceImpl
{
public:
  MisakaTexLayerImpl(KawaiiTexLayer *model);
  ~MisakaTexLayerImpl();

  // KawaiiImageSourceImpl interface
public:
  void directDraw() override final;

protected:
  void drawQuad() override final;



  // IMPLEMENT
private:
  MisakaRootImpl *root;
  MisakaTextureHandle *attachedTexHandle;

  void setRoot(KawaiiRoot *newRoot);

  uint32_t getModelLayersCount() const;

  // KawaiiRendererImpl interface
private:
  void initConnections() override final;
};

#endif // MISAKATEXLAYERIMPL_HPP
