#ifndef MISAKAQPAINTERLAYERIMPL_HPP
#define MISAKAQPAINTERLAYERIMPL_HPP

#include "MisakaImageSourceImpl.hpp"
#include <KawaiiRenderer/Renderpass/KawaiiQPainterLayerImpl.hpp>
#include <QOpenGLPaintDevice>

class MISAKARENDERER_SHARED_EXPORT MisakaQPainterLayerImpl : public MisakaImageSourceImpl, public KawaiiQPainterLayerImpl
{
public:
  MisakaQPainterLayerImpl(KawaiiQPainterLayer *model);
  ~MisakaQPainterLayerImpl();

  // KawaiiImageSourceImpl interface
protected:
  void drawQuad() override final;

  // KawaiiQPainterLayerImpl interface
protected:
  void refreshPixmaps() override final;



  // IMPLEMENT
private:
  std::unique_ptr<QPainter> qpainter;
  std::unique_ptr<QOpenGLPaintDevice> paintDev;

  QPainter *getQPainter();
};

#endif // MISAKAQPAINTERLAYERIMPL_HPP
