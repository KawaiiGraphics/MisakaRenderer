#include "MisakaQPainterLayerImpl.hpp"
#include "MisakaImageSourceImpl.hpp"

MisakaQPainterLayerImpl::MisakaQPainterLayerImpl(KawaiiQPainterLayer *model):
  KawaiiImageSourceImpl(model),
  MisakaImageSourceImpl(model),
  KawaiiQPainterLayerImpl(model)
{
  painterCreater = std::bind(&MisakaQPainterLayerImpl::getQPainter, this);
}

MisakaQPainterLayerImpl::~MisakaQPainterLayerImpl()
{
}

void MisakaQPainterLayerImpl::drawQuad()
{ }

void MisakaQPainterLayerImpl::refreshPixmaps()
{ }

QPainter *MisakaQPainterLayerImpl::getQPainter()
{
  if(!paintDev)
    paintDev = std::make_unique<QOpenGLPaintDevice>(QSize(getModel()->getRenderableSize().x, getModel()->getRenderableSize().y));
  else
    paintDev->setSize(QSize(getModel()->getRenderableSize().x, getModel()->getRenderableSize().y));

  if(!qpainter)
    qpainter = std::make_unique<QPainter>(paintDev.get());
  else
    qpainter->begin(paintDev.get());
  return qpainter.get();
}
