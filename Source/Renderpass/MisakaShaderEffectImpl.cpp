#include "MisakaShaderEffectImpl.hpp"
#include "MisakaRootImpl.hpp"

MisakaShaderEffectImpl::MisakaShaderEffectImpl(KawaiiShaderEffect *model):
  KawaiiImageSourceImpl(model),
  MisakaImageSourceImpl(model),
  KawaiiShaderEffectImpl(model)
{
  connect(model, &sib_utils::TreeNode::parentUpdated, this, [this] { setRoot(KawaiiRoot::getRoot(getModel())); });
  setRoot(KawaiiRoot::getRoot(getModel()));
}

void MisakaShaderEffectImpl::drawQuad()
{
  root->drawOverlayQuad();
}

void MisakaShaderEffectImpl::setRoot(KawaiiRoot *newRoot)
{
  if(!newRoot)
    root = nullptr;
  else
    root = static_cast<MisakaRootImpl*>(newRoot->getRendererImpl());
}
