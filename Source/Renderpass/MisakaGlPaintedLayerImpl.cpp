#include "MisakaGlPaintedLayerImpl.hpp"

MisakaGlPaintedLayerImpl::MisakaGlPaintedLayerImpl(KawaiiGlPaintedLayer *model):
  KawaiiImageSourceImpl(model),
  MisakaImageSourceImpl(model),
  KawaiiGlPaintedLayerImpl(model)
{
  ctxGetter = std::bind(&MisakaGlPaintedLayerImpl::getGlCtx, this);
}

void MisakaGlPaintedLayerImpl::drawQuad()
{ }

void MisakaGlPaintedLayerImpl::refreshPixmaps()
{ }

QOpenGLContext *MisakaGlPaintedLayerImpl::getGlCtx()
{
  return QOpenGLContext::currentContext();
}
