#include "MisakaImageSourceImpl.hpp"
#include <KawaiiRenderer/Renderpass/KawaiiImageSourceImpl.hpp>

MisakaImageSourceImpl::MisakaImageSourceImpl(KawaiiImageSource *model):
  KawaiiImageSourceImpl(model)
{
}

MisakaImageSourceImpl::~MisakaImageSourceImpl()
{
}

void MisakaImageSourceImpl::createCache()
{
  Q_UNREACHABLE();
}

void MisakaImageSourceImpl::drawCache()
{
  Q_UNREACHABLE();
}

void MisakaImageSourceImpl::updateCache()
{
  Q_UNREACHABLE();
}

void MisakaImageSourceImpl::deleteCache()
{
  Q_UNREACHABLE();
}
