#ifndef MISAKARENDERPASSIMPL_HPP
#define MISAKARENDERPASSIMPL_HPP

#include <KawaiiRenderer/Renderpass/KawaiiRenderpassImpl.hpp>
#include "../MisakaRenderer_global.hpp"
#include <QOpenGLPaintDevice>

class MisakaRootImpl;
class MISAKARENDERER_SHARED_EXPORT MisakaRenderpassImpl: public KawaiiRenderpassImpl
{
public:
  struct ExternalGBufDescr
  {
    GLuint glTextureId;
    GLint layer;
  };

  MisakaRenderpassImpl(KawaiiRenderpass *renderpass);
  ~MisakaRenderpassImpl();

  void setExternalGbuf(size_t index, GLuint glTextureId, GLint layer);
  void unuseExternalGbufs();

  void blitSfcColor(GLint dstX, GLint dstY, GLint dstWidth, GLint dstHeight);

  bool getLaziness() const;

  // KawaiiRenderpassImpl interface
private:
  void create() override final;
  void destroy() override final;
  bool start() override final;
  void nextSubpass() override final;
  void finish() override final;
  void drawCache() override final;



  // IMPLEMENT
private:
  MisakaRootImpl *root;
  std::vector<ExternalGBufDescr> externalGBufs;
  std::vector<GLuint> internalGBufs;
  size_t activeColorAttachments;
  GLuint glFboId;

  std::vector<float> readGbufF (uint32_t gbuf, const QRect &reg);
  std::vector<int32_t> readGbufI (uint32_t gbuf, const QRect &reg);
  std::vector<uint32_t> readGbufU (uint32_t gbuf, const QRect &reg);

  void setRoot(MisakaRootImpl *newRoot);

  void onModelParentChanged();

  GLenum getGBufInternalFmt(size_t gbufIndex);

  void activateSubpass(size_t subpassIndex);

  ExternalGBufDescr getGBufDescr(size_t gbufIndex) const;
  GLenum getGBufReadFmt(size_t gbufIndex) const;
  uint64_t getGBufReadFmtComponents(size_t gbufIndex) const;
  GLenum getGBufReadAttachment(size_t gbufIndex) const;

  void attachTexture(GLenum attachement, const ExternalGBufDescr &gbufDescr);

  void clearGBuf(size_t gbufIndex);
};

#endif // MISAKARENDERPASSIMPL_HPP
