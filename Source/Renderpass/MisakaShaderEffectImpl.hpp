#ifndef MISAKASHADEREFFECTIMPL_HPP
#define MISAKASHADEREFFECTIMPL_HPP

#include "MisakaImageSourceImpl.hpp"
#include <KawaiiRenderer/Renderpass/KawaiiShaderEffectImpl.hpp>
#include <QOpenGLVertexArrayObject>

class MisakaRootImpl;
class MISAKARENDERER_SHARED_EXPORT MisakaShaderEffectImpl: public MisakaImageSourceImpl, public KawaiiShaderEffectImpl
{
public:
  MisakaShaderEffectImpl(KawaiiShaderEffect *model);

  // KawaiiImageSourceImpl interface
protected:
  void drawQuad() override final;



  // IMPLEMENT
private:
  MisakaRootImpl *root;

  void setRoot(KawaiiRoot *newRoot);
};

#endif // MISAKASHADEREFFECTIMPL_HPP
