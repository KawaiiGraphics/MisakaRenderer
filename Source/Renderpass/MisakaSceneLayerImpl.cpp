#include "MisakaSceneLayerImpl.hpp"

MisakaSceneLayerImpl::MisakaSceneLayerImpl(KawaiiSceneLayer *model):
  KawaiiImageSourceImpl(model),
  MisakaImageSourceImpl(model),
  KawaiiSceneLayerImpl(model)
{
  model->setClipCorrection(glm::mat4(1.0));
}

MisakaSceneLayerImpl::~MisakaSceneLayerImpl()
{
}

void MisakaSceneLayerImpl::drawQuad()
{ }
