#include "MisakaGpuBufImpl.hpp"
#include "MisakaRootImpl.hpp"
#include "Textures/MisakaTextureHandle.hpp"

#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>

#include <Kawaii3D/KawaiiConfig.hpp>

#include <functional>
#include <cstring>

MisakaGpuBufImpl::MisakaGpuBufImpl(KawaiiGpuBuf *model):
  KawaiiGpuBufImpl(model),
  texturesUbo(nullptr)
{
  if(model)
    {
      static_cast<MisakaBufferHandle*>(getHandle())->setUboPostbindFunc(std::bind(&MisakaGpuBufImpl::bindUboTextures, this, std::placeholders::_1, std::placeholders::_2));
      onRootChanged = std::bind(&MisakaGpuBufImpl::rootChanged, this);
      rootChanged();

      connect(model, &KawaiiGpuBuf::textureBinded, this, &MisakaGpuBufImpl::bindTexture);
    } else
    deleteLater();
}

MisakaGpuBufImpl::~MisakaGpuBufImpl()
{
  destroyTexturesUbo();
}

void MisakaGpuBufImpl::bindTexture(const QString &fieldName, KawaiiTexture *tex)
{
  const std::string std_fieldName = fieldName.toStdString() + "_handle";
  auto el = bindedTextures.find(std_fieldName);
  if(tex)
    {
      auto *texImpl = static_cast<KawaiiTextureImpl*>(tex->getRendererImpl());
      if(el == bindedTextures.end())
        {
          el = bindedTextures.insert({std_fieldName, texImpl}).first;
          rebindUbo();
        } else if(el->second != texImpl)
        {
          el->second = texImpl;
          rebindUbo();
        }
    } else
    {
      if(el == bindedTextures.end())
        el = bindedTextures.insert({std_fieldName, nullptr}).first;
      else
        el->second.clear();
      rebindUbo();
    }
}

void MisakaGpuBufImpl::bindUboTextures(GLenum target, GLuint bindingPoint)
{
  const bool isGlobalBinding = target != GL_UNIFORM_BUFFER || bindingPoint > KawaiiShader::getUboCount();
  if(isGlobalBinding)
    {
      bindedUbo.emplace(target, bindingPoint);
      static_cast<MisakaRootImpl*>(getRoot())->setBindedUbo(target, bindingPoint, this, KawaiiBufferTarget::UBO);
    }

  if(!bindedTextures.empty() && getRoot())
    {
      if(isGlobalBinding)
        bindingPoint = KawaiiShader::getUboCount() + 1;

      auto &gl = static_cast<MisakaRootImpl*>(getRoot())->gl();

      GLint prog;
      gl.glGetIntegerv(GL_CURRENT_PROGRAM, &prog);
      if(prog)
        sendTextures(bindingPoint, prog);
      else
        static_cast<MisakaRootImpl*>(getRoot())->addOnShaderProgUsed([this, bindingPoint] (GLint prog) { sendTextures(bindingPoint, prog); } );
    }
  if(texturesUbo)
    texturesUbo->bindToBlock(KawaiiBufferTarget::UBO, bindingPoint + KawaiiShader::getUboCount());
}

size_t MisakaGpuBufImpl::getTexturesUboSize(GLuint bindingPoint, GLint progId)
{
  if(bindingPoint <= KawaiiShader::getUboCount())
    bindingPoint += KawaiiShader::getUboCount();
  else
    bindingPoint = 2*KawaiiShader::getUboCount() + 1;

  auto &gl = static_cast<MisakaRootImpl*>(getRoot())->gl();

  //Get uniform block count in the program
  GLint blockCount;
  gl.glGetProgramiv(progId, GL_ACTIVE_UNIFORM_BLOCKS, &blockCount);
  if(blockCount < 0)
    return 0;

  //Get a block index, assigned to the buffer at bindingPoint
  for(GLuint uniformBlockIndex = 0; uniformBlockIndex < static_cast<GLuint>(blockCount); ++uniformBlockIndex)
    {
      GLint tempBindingPoint = 0;
      gl.glGetActiveUniformBlockiv(progId, uniformBlockIndex, GL_UNIFORM_BLOCK_BINDING, &tempBindingPoint);
      if(tempBindingPoint > 0 && static_cast<GLuint>(tempBindingPoint) == bindingPoint)
        {
          //Get size of the textures uniform block
          GLint askedSize = 0;
          gl.glGetActiveUniformBlockiv(progId, uniformBlockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &askedSize);

          return static_cast<size_t>(std::max(askedSize, 0));
        }
    }

  return 0;
}

void MisakaGpuBufImpl::sendTextures(GLuint bindingPoint, GLint progId)
{
  if(bindingPoint == KawaiiShader::getUboCount() + 1)
    sendTextures(static_cast<MisakaRootImpl*>(getRoot())->getGlobalTexturesUbo(), bindingPoint, progId);
  else
    {
      if(!texturesUbo)
        {
          texturesUbo = static_cast<MisakaBufferHandle*>( static_cast<MisakaRootImpl*>(getRoot())->createBuffer(nullptr, 0, {KawaiiBufferTarget::UBO}) );
          texturesUbo->setOnInvalidate([this] {
              texturesUbo = nullptr;
              for(auto &i: texturesInUbo)
                if(bindedTextures.count(i.first) == 0 && i.second)
                  bindedTextures[i.first] = i.second;
              qDebug().noquote() << QLatin1String(" reallocated textures ubo for ") << this;
            });
        }
      sendTextures(*texturesUbo, bindingPoint, progId);
    }
  if(texturesUbo)
    texturesUbo->bindToBlock(KawaiiBufferTarget::UBO, bindingPoint + KawaiiShader::getUboCount());
}

void MisakaGpuBufImpl::sendTextures(MisakaBufferHandle &texturesUbo, GLuint bindingPoint, GLint progId)
{
  auto &gl = static_cast<MisakaRootImpl*>(getRoot())->gl();

  if(bindedTextures.empty())
    return;

  const size_t texturesUboSzRequired = getTexturesUboSize(bindingPoint, progId);
  if(texturesUbo.getBufferSize() < texturesUboSzRequired)
    {
      bool wasEmpty = (texturesUbo.getBufferSize() == 0);
      std::vector<std::byte> buffer(texturesUboSzRequired, static_cast<std::byte>(0));
      texturesUbo.setData(buffer);
      if(!wasEmpty)
        {
          static_cast<MisakaRootImpl*>(getRoot())->addOnShaderProgUsed([this, bindingPoint] (GLint prog) { sendTextures(bindingPoint, prog); } );
          return;
        }
    }

  std::vector<const GLchar*> uniformNames(bindedTextures.size());
  auto namIter = uniformNames.begin();

  for(const auto &i: bindedTextures)
    {
      *namIter = reinterpret_cast<const GLchar*>(i.first.c_str());
      ++namIter;
    }

  std::vector<GLuint> indices(bindedTextures.size());
  gl.glGetUniformIndices(progId, bindedTextures.size(), &uniformNames[0], &indices[0]);

  std::vector<GLint> offsets(indices.size(), -1);
  for(size_t i = 0; i < offsets.size(); ++i)
    if(indices[i] != std::numeric_limits<GLuint>::max())
      gl.glGetActiveUniformsiv(progId, 1, &indices[i], GL_UNIFORM_OFFSET, &offsets[i]);

  auto offsetsIter = offsets.begin();
  for(auto i = bindedTextures.begin(); i != bindedTextures.end(); )
    {
      GLint offset = *(offsetsIter++);
      if(offset > -1 && sizeof(GLuint64) + offset <= texturesUboSzRequired)
        {
          const GLuint64 texHandle = i->second? static_cast<MisakaTextureHandle*>(i->second->getHandle())->getTexHandle(): 0;
          texturesUbo.setSubData(offset, &texHandle, sizeof(GLuint64));
          texturesInUbo[i->first] = i->second;
          i = bindedTextures.erase(i);
        } else
        ++i;
    }

  if(!bindedTextures.empty())
    static_cast<MisakaRootImpl*>(getRoot())->addOnShaderProgUsed([this, bindingPoint] (GLint prog) { sendTextures(bindingPoint, prog); } );
}

void MisakaGpuBufImpl::rootChanged()
{
  destroyTexturesUbo();
}

void MisakaGpuBufImpl::destroyTexturesUbo()
{
  if(texturesUbo)
    {
      delete texturesUbo;
      texturesUbo = nullptr;
    }
}

void MisakaGpuBufImpl::rebindUbo()
{
  for(const auto &i: bindedUbo)
    if(static_cast<MisakaRootImpl*>(getRoot())->isUboBinded(i.first, i.second, this))
      bindUboTextures(i.first, i.second);
}

void MisakaGpuBufImpl::initConnections()
{
  getModel()->forallTextureBindings(std::bind(&MisakaGpuBufImpl::bindTexture, this, std::placeholders::_1, std::placeholders::_2));
}
