#include "KawaiiMisakaFactory.hpp"

#include "MisakaRootImpl.hpp"
#include "Surfaces/MisakaSurfaceImpl.hpp"
#include "MisakaGpuBufImpl.hpp"
#include "Geometry/MisakaMeshImpl.hpp"
#include "Renderpass/MisakaRenderpassImpl.hpp"
#include "Renderpass/MisakaSceneLayerImpl.hpp"
#include "Renderpass/MisakaGlPaintedLayerImpl.hpp"
#include "Renderpass/MisakaQPainterLayerImpl.hpp"
#include "Renderpass/MisakaShaderEffectImpl.hpp"
#include "Renderpass/MisakaTexLayerImpl.hpp"
#include "Shaders/MisakaShaderImpl.hpp"
#include "Shaders/MisakaProgramImpl.hpp"
#include <KawaiiRenderer/KawaiiBufBindingImpl.hpp>
#include <KawaiiRenderer/Geometry/KawaiiMeshInstanceImpl.hpp>
#include <KawaiiRenderer/KawaiiMaterialImpl.hpp>
#include <KawaiiRenderer/Shaders/KawaiiSceneImpl.hpp>
#include <KawaiiRenderer/KawaiiCameraImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiCubemapImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiImgImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiDepthCubemapArrayImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiDepthTex2dArrayImpl.hpp>
#include "Textures/MisakaEnvMapImpl.hpp"
#include "Textures/MisakaExternalQRhiRender.hpp"
#include "Textures/MisakaFramebufferImpl.hpp"

#include <QCoreApplication>

KawaiiMisakaFactory *KawaiiMisakaFactory::instance = nullptr;

KawaiiMisakaFactory::KawaiiMisakaFactory()
{
  registerImpl<KawaiiRoot,               MisakaRootImpl>              ();
  registerImpl<KawaiiSurface,            MisakaSurfaceImpl>           ();
  registerImpl<KawaiiGpuBuf,             MisakaGpuBufImpl>            ();
  registerImpl<KawaiiBufBinding,         KawaiiBufBindingImpl>        ();
  registerImpl<KawaiiMesh3D,             MisakaMeshImpl>              ();
  registerImpl<KawaiiRenderpass,         MisakaRenderpassImpl>        ();
  registerImpl<KawaiiSceneLayer,         MisakaSceneLayerImpl>        ();
  registerImpl<KawaiiGlPaintedLayer,     MisakaGlPaintedLayerImpl>    ();
  registerImpl<KawaiiQPainterLayer,      MisakaQPainterLayerImpl>     ();
  registerImpl<KawaiiShaderEffect,       MisakaShaderEffectImpl>      ();
  registerImpl<KawaiiTexLayer,           MisakaTexLayerImpl>          ();
  registerImpl<KawaiiShader,             MisakaShaderImpl>            ();
  registerImpl<KawaiiProgram,            MisakaProgramImpl>           ();
  registerImpl<KawaiiMeshInstance,       KawaiiMeshInstanceImpl>      ();
  registerImpl<KawaiiMaterial,           KawaiiMaterialImpl>          ();
  registerImpl<KawaiiScene,              KawaiiSceneImpl>             ();
  registerImpl<KawaiiCamera,             KawaiiCameraImpl>            ();
  registerImpl<KawaiiCubemap,            KawaiiCubemapImpl>           ();
  registerImpl<KawaiiImage,              KawaiiImgImpl>               ();
  registerImpl<KawaiiDepthCubemapArray,  KawaiiDepthCubemapArrayImpl> ();
  registerImpl<KawaiiDepthTex2dArray,    KawaiiDepthTex2dArrayImpl>   ();
  registerImpl<KawaiiEnvMap,             MisakaEnvMapImpl>            ();
  registerImpl<KawaiiExternalQRhiRender, MisakaExternalQRhiRender>    ();
  registerImpl<KawaiiFramebuffer,        MisakaFramebufferImpl>       ();
}

KawaiiImplFactory *KawaiiMisakaFactory::getInstance()
{
  if(!instance)
    createInstance();
  return instance;
}

void KawaiiMisakaFactory::deleteInstance()
{
  if(instance)
    {
      instance->deleteLater();
      instance = nullptr;
    }
}

void KawaiiMisakaFactory::createInstance()
{
  instance = new KawaiiMisakaFactory;
  qAddPostRoutine(&KawaiiMisakaFactory::deleteInstance);
}
