#ifndef KAWAIIMISAKAFACTORY_HPP
#define KAWAIIMISAKAFACTORY_HPP

#include <Kawaii3D/KawaiiImplFactory.hpp>
#include "MisakaRenderer_global.hpp"

class MISAKARENDERER_SHARED_EXPORT KawaiiMisakaFactory : public KawaiiImplFactory
{
  KawaiiMisakaFactory();
  ~KawaiiMisakaFactory() = default;

public:
  static KawaiiImplFactory* getInstance();
  static void deleteInstance();



  //IMPLEMENT
private:
  static KawaiiMisakaFactory *instance;
  static void createInstance();
};

#endif // KAWAIIMISAKAFACTORY_HPP
