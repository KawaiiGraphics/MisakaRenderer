#ifndef MISAKAGPUBUFIMPL_HPP
#define MISAKAGPUBUFIMPL_HPP

#include <KawaiiRenderer/KawaiiGpuBufImpl.hpp>
#include "MisakaBufferHandle.hpp"
#include <unordered_map>
#include <unordered_set>
#include <qopengl.h>
#include <QPointer>

class MISAKARENDERER_SHARED_EXPORT MisakaGpuBufImpl : public KawaiiGpuBufImpl
{
public:
  MisakaGpuBufImpl(KawaiiGpuBuf *model);
  ~MisakaGpuBufImpl();

  void bindTexture(const QString &fieldName, KawaiiTexture *tex);



  //IMPLEMENT
private:
  std::unordered_map<std::string, QPointer<KawaiiTextureImpl>> bindedTextures;
  std::unordered_map<std::string, QPointer<KawaiiTextureImpl>> texturesInUbo;
  MisakaBufferHandle *texturesUbo;
  std::unordered_set<std::pair<GLenum, GLuint>, sib_utils::PairHash<GLenum, GLuint>> bindedUbo;


  void bindUboTextures(GLenum target, GLuint bindingPoint);
  size_t getTexturesUboSize(GLuint bindingPoint, GLint progId);

  void sendTextures(GLuint bindingPoint, GLint progId);
  void sendTextures(MisakaBufferHandle &texturesUbo, GLuint bindingPoint, GLint progId);

  void rootChanged();
  void destroyTexturesUbo();

  void rebindUbo();

  // KawaiiRendererImpl interface
private:
  void initConnections() override final;
};

#endif // MISAKAGPUBUFIMPL_HPP
