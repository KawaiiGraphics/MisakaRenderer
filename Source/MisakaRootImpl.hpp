#ifndef MISAKAROOTIMPL_HPP
#define MISAKAROOTIMPL_HPP

#include <KawaiiRenderer/KawaiiRootImpl.hpp>

#include "QOpenGLExtension_ARB_bindless_texture.hpp"
#include "MisakaBufferHandle.hpp"

#include <vector>

#include <qopengl.h>
#include <rhi/qrhi.h>
#include <QOpenGLContext>
#include <QOffscreenSurface>
#include <QOpenGLPaintDevice>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLFunctions_4_5_Core>

class MISAKARENDERER_SHARED_EXPORT MisakaRootImpl: public KawaiiRootImpl
{
public:
  MisakaRootImpl(KawaiiRoot *model);
  ~MisakaRootImpl();

  void taskGL(const std::function<void()> &func);
  void asyncTaskGL(const std::function<void()> &func);

  static QSurfaceFormat requestedFormat();

  QOpenGLFunctions_4_5_Core& gl() const;

  QOpenGLExtension_ARB_bindless_texture& ARB_bindless_texture();

  MisakaBufferHandle& getGlobalTexturesUbo();

  void addOnShaderProgUsed(const std::function<void(GLint)> &func);

  void useShaderProg(GLint prog);

  float getGlFloat(GLenum pname);

  const QString &getShader5GlslExtension() const;

  void makeCtxCurrent(QSurface *wnd);
  void startRendering(QWindow *wnd);
  void finishRendering(QWindow *wnd);

  void setBindedUbo(GLenum target, GLuint bindingPoint, void *buf, KawaiiBufferTarget bufferTarget);
  bool isUboBinded(GLenum target, GLuint bindingPoint, void *buf) const;

  void glEnable(GLenum cap, bool enabled);

  void setBindedInputGbufs(const std::vector<GLuint> &newBindedInputGbufs);
  void setBindedSampledGbufs(const std::vector<GLuint> &newBindedSampledGbufs);
  void setBindedStorageGbufs(const std::vector<GLuint> &newBindedStorageGbufs);
  void clearBindedGBufs();

  QOpenGLShaderProgram& getArrayedOverlayShaders(uint32_t layersCount);
  void drawOverlayQuad();

  QRhi& getQRhi();

  void rebindGlobalTexturesUbo();

  void finalize(const QSize &sz);

  static bool checkSystem();

  // KawaiiRootImpl interface
public:
  KawaiiBufferHandle *createBuffer(const void *ptr, size_t n, const std::initializer_list<KawaiiBufferTarget> &availableTargets) override final;
  KawaiiTextureHandle *createTexture(KawaiiTexture *model) override final;

private:
  void beginOffscreen() override final;
  void endOffscreen() override final;



  //IMPLEMENT
private:
  QOpenGLContext glCtx;

  QOffscreenSurface sfcStub;
  QOpenGLExtension_ARB_bindless_texture bindlessTexture;

  std::unordered_map<GLenum, GLfloat> glFloatParams;

  std::unique_ptr<MisakaBufferHandle> globalTexturesUbo;
  std::unique_ptr<MisakaBufferHandle> gbufTexturesUbo;
  std::vector<std::function<void(GLint)>> onShaderProgUsed;

  std::vector<GLuint> bindedInputGbufs;
  std::vector<GLuint> bindedSampledGbufs;
  std::vector<GLuint> bindedStorageGbufs;

  QOpenGLFunctions_4_5_Core *glFuncs;
  QString glsl5Extension;

  std::unordered_map<std::pair<GLenum, GLuint>, std::pair<void*, KawaiiBufferTarget>, sib_utils::PairHash<GLenum, GLuint>> bindedUbo;

  std::unique_ptr<QOpenGLPaintDevice> qPaintDev;
  std::unique_ptr<QPainter> qpainter;
  std::unique_ptr<QRhi> qrhi;

  std::unique_ptr<QOpenGLShaderProgram> quadShader;
  std::unordered_map<uint32_t, std::unique_ptr<QOpenGLShaderProgram>> quadArrayedShaders;

  std::unique_ptr<QOpenGLVertexArrayObject> quadVao;

  bool globTexturesBufBinded;

  static void receiveGLDebug(GLenum source, GLenum type, GLuint id,
                             GLenum severity, GLsizei length, const GLchar* message, const void*);


  Q_INVOKABLE void initContext();
  Q_INVOKABLE void deleteContext();
};

#endif // MISAKAROOTIMPL_HPP
