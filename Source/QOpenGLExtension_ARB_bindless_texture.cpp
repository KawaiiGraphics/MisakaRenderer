#include "QOpenGLExtension_ARB_bindless_texture.hpp"

QOpenGLExtension_ARB_bindless_texture::QOpenGLExtension_ARB_bindless_texture():
  arbBindlessTexture(nullptr)
{
}

QOpenGLExtension_ARB_bindless_texture::~QOpenGLExtension_ARB_bindless_texture()
{
  if(arbBindlessTexture)
    delete arbBindlessTexture;
}

GLboolean QOpenGLExtension_ARB_bindless_texture::glIsImageHandleResident(GLuint64 handle)
{
  if(Q_LIKELY(arbBindlessTexture))
    return arbBindlessTexture->isImageHandleResidentARB(handle);
  else
    return GL_FALSE;
}

GLboolean QOpenGLExtension_ARB_bindless_texture::glIsTextureHandleResident(GLuint64 handle)
{
  if(Q_LIKELY(arbBindlessTexture))
    return arbBindlessTexture->isTextureHandleResidentARB(handle);
  else
    return GL_FALSE;
}

void QOpenGLExtension_ARB_bindless_texture::glProgramUniformHandleui64v(GLuint program, GLint location, GLsizei count, const GLuint64 *values)
{
  if(Q_LIKELY(arbBindlessTexture))
    arbBindlessTexture->programUniformHandleui64vARB(program, location, count, values);
}

void QOpenGLExtension_ARB_bindless_texture::glProgramUniformHandleui64(GLuint program, GLint location, GLuint64 value)
{
  if(Q_LIKELY(arbBindlessTexture))
    arbBindlessTexture->programUniformHandleui64ARB(program, location, value);
}

void QOpenGLExtension_ARB_bindless_texture::glUniformHandleui64v(GLint location, GLsizei count, const GLuint64 *value)
{
  if(Q_LIKELY(arbBindlessTexture))
    arbBindlessTexture->uniformHandleui64vARB(location, count, value);
}

void QOpenGLExtension_ARB_bindless_texture::glUniformHandleui64(GLint location, GLuint64 value)
{
  if(Q_LIKELY(arbBindlessTexture))
    arbBindlessTexture->uniformHandleui64ARB(location, value);
}

void QOpenGLExtension_ARB_bindless_texture::glMakeImageHandleNonResident(GLuint64 handle)
{
  if(Q_LIKELY(arbBindlessTexture))
    arbBindlessTexture->makeImageHandleNonResidentARB(handle);
}

void QOpenGLExtension_ARB_bindless_texture::glMakeImageHandleResident(GLuint64 handle, GLenum access)
{
  if(Q_LIKELY(arbBindlessTexture))
    arbBindlessTexture->makeImageHandleResidentARB(handle, access);
}

GLuint64 QOpenGLExtension_ARB_bindless_texture::glGetImageHandle(GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format)
{
  if(Q_LIKELY(arbBindlessTexture))
    return arbBindlessTexture->getImageHandleARB(texture, level, layered, layer, format);
  else
    return 0;
}

void QOpenGLExtension_ARB_bindless_texture::glMakeTextureHandleNonResident(GLuint64 handle)
{
  if(Q_LIKELY(arbBindlessTexture))
    arbBindlessTexture->makeTextureHandleNonResidentARB(handle);
}

void QOpenGLExtension_ARB_bindless_texture::glMakeTextureHandleResident(GLuint64 handle)
{
  if(Q_LIKELY(arbBindlessTexture))
    arbBindlessTexture->makeTextureHandleResidentARB(handle);
}

GLuint64 QOpenGLExtension_ARB_bindless_texture::glGetTextureSamplerHandle(GLuint texture, GLuint sampler)
{
  if(Q_LIKELY(arbBindlessTexture))
    return arbBindlessTexture->getTextureSamplerHandleARB(texture, sampler);
  else
    return 0;
}

GLuint64 QOpenGLExtension_ARB_bindless_texture::glGetTextureHandle(GLuint texture)
{
  if(Q_LIKELY(arbBindlessTexture))
    return arbBindlessTexture->getTextureHandleARB(texture);
  else
    return 0;
}

bool QOpenGLExtension_ARB_bindless_texture::initializeOpenGLFunctions()
{
  if(Q_UNLIKELY(arbBindlessTexture))
    return true;

  QOpenGLContext *ctx = QOpenGLContext::currentContext();
  if (!ctx)
    {
      qWarning("A current OpenGL context is required to resolve OpenGL extension functions");
      return false;
    }

  if(!ctx->extensions().contains("GL_ARB_bindless_texture"))
    {
      qWarning("GL_ARB_bindless_texture is not supported");
      return false;
    }

  auto getProcAddress = [ctx](auto &proc, const char *procName) {
      proc = reinterpret_cast<typename std::remove_reference_t<decltype(proc)>>(ctx->getProcAddress(procName));
    };

  arbBindlessTexture = new QOpenGLExtension_ARB_bindless_texturePrivate;
  getProcAddress(arbBindlessTexture->isImageHandleResidentARB,       "glIsImageHandleResidentARB");
  getProcAddress(arbBindlessTexture->isTextureHandleResidentARB,     "glIsTextureHandleResidentARB");
  getProcAddress(arbBindlessTexture->programUniformHandleui64vARB,   "glProgramUniformHandleui64vARB");
  getProcAddress(arbBindlessTexture->programUniformHandleui64ARB,    "glProgramUniformHandleui64ARB");
  getProcAddress(arbBindlessTexture->uniformHandleui64vARB,          "glUniformHandleui64vARB");
  getProcAddress(arbBindlessTexture->uniformHandleui64ARB,           "glUniformHandleui64ARB");
  getProcAddress(arbBindlessTexture->makeImageHandleNonResidentARB,  "glMakeImageHandleNonResidentARB");
  getProcAddress(arbBindlessTexture->makeImageHandleResidentARB,     "glMakeImageHandleResidentARB");
  getProcAddress(arbBindlessTexture->getImageHandleARB,              "glGetImageHandleARB");
  getProcAddress(arbBindlessTexture->makeTextureHandleNonResidentARB,"glMakeTextureHandleNonResidentARB");
  getProcAddress(arbBindlessTexture->makeTextureHandleResidentARB,   "glMakeTextureHandleResidentARB");
  getProcAddress(arbBindlessTexture->getTextureSamplerHandleARB,     "glGetTextureSamplerHandleARB");
  getProcAddress(arbBindlessTexture->getTextureHandleARB,            "glGetTextureHandleARB");

  return true;
}
