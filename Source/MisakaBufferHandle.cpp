#include "MisakaBufferHandle.hpp"
#include "MisakaRootImpl.hpp"

template class sib_utils::PairHash<GLenum, GLuint>;

MisakaBufferHandle::MisakaBufferHandle(const void *ptr, size_t n, MisakaRootImpl &root):
  r(root),
  data(ptr),
  length(n),
  id(0)
{
  r.taskGL([this] {
      r.gl().glCreateBuffers(1, &id);
      if(length)
        r.gl().glNamedBufferStorage(id, length, data, GL_DYNAMIC_STORAGE_BIT);
    });
}

MisakaBufferHandle::~MisakaBufferHandle()
{
  r.taskGL([this] {
      r.gl().glDeleteBuffers(1, &id);
    });
}

void MisakaBufferHandle::setUboPostbindFunc(const std::function<void (GLenum, uint32_t)> &func)
{
  uboPostbind = func;
}

void MisakaBufferHandle::bindVbo(GLuint vao_id, GLuint bindingIndex, GLintptr offset, GLsizei stride) const
{
  r.gl().glVertexArrayVertexBuffer(vao_id, bindingIndex, id, offset, stride);
}

void MisakaBufferHandle::bindElementBuffer(GLuint vao_id) const
{
  r.gl().glVertexArrayElementBuffer(vao_id, id);
}

void MisakaBufferHandle::setSubData(size_t offset, const void *ptr, size_t n)
{
  Q_ASSERT(offset + n <= length);
  r.asyncTaskGL(std::bind(&MisakaBufferHandle::setSubDataWork, this, offset, ptr, n));
}

void MisakaBufferHandle::setBufferData(const void *ptr, size_t n)
{
  if(length && length != n)
    invalidate();
  else
    r.asyncTaskGL(std::bind(&MisakaBufferHandle::setBufferDataWork, this, ptr, n));
}

void MisakaBufferHandle::sendChunkToGpu(size_t offset, size_t n)
{
  setSubData(offset, static_cast<const std::byte*>(data) + offset, n);
}

size_t MisakaBufferHandle::getBufferSize() const
{
  return length;
}

void MisakaBufferHandle::bind(KawaiiBufferTarget target)
{
  r.taskGL(std::bind(&MisakaBufferHandle::bindWork, this, target));
}

void MisakaBufferHandle::unbind(KawaiiBufferTarget target)
{
  r.taskGL(std::bind(&MisakaBufferHandle::unbindWork, this, target));
}

void MisakaBufferHandle::bindToBlock(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  r.taskGL(std::bind(&MisakaBufferHandle::bindToBlockWork, this, target, bindingPoint));
}

void MisakaBufferHandle::userBinding(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  if(target == KawaiiBufferTarget::UBO)
    bindToBlock(target, KawaiiShader::getUboCount() * 2 + 1 + bindingPoint);
  else
    bindToBlock(target, bindingPoint);
}

void MisakaBufferHandle::unbindFromBlock(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  bindingPoint += KawaiiShader::getUboCount() * 2 + 1;
  r.taskGL(std::bind(&MisakaBufferHandle::unbindFromBlockWork, this, target, bindingPoint));
}

void MisakaBufferHandle::setBufferDataWork(const void *ptr, size_t n)
{
  if(!length && n)
    r.gl().glNamedBufferStorage(id, n, ptr, GL_DYNAMIC_STORAGE_BIT);
  else
    if(ptr != data)
      r.gl().glNamedBufferSubData(id, 0, n, ptr);

  data = ptr;
  length = n;
}

void MisakaBufferHandle::setSubDataWork(size_t offset, const void *ptr, size_t n)
{
  r.gl().glNamedBufferSubData(id, offset, n, ptr);
}

namespace {
  GLenum trBufferTarget(KawaiiBufferTarget target)
  {
    switch(target)
      {
      case KawaiiBufferTarget::UBO:
        return GL_UNIFORM_BUFFER;

      case KawaiiBufferTarget::VBO:
        return GL_ARRAY_BUFFER;

      case KawaiiBufferTarget::SSBO:
        return GL_SHADER_STORAGE_BUFFER;

      case KawaiiBufferTarget::CopyRead:
        return GL_COPY_READ_BUFFER;

      case KawaiiBufferTarget::CopyWrite:
        return GL_COPY_WRITE_BUFFER;

      case KawaiiBufferTarget::PixelPack:
        return GL_PIXEL_PACK_BUFFER;

      case KawaiiBufferTarget::PixelUnpack:
        return GL_PIXEL_UNPACK_BUFFER;

      case KawaiiBufferTarget::QueryBuffer:
        return GL_QUERY_BUFFER;

      case KawaiiBufferTarget::DrawIndirect:
        return GL_DRAW_INDIRECT_BUFFER;

      case KawaiiBufferTarget::AtomicCounter:
        return GL_ATOMIC_COUNTER_BUFFER;

      case KawaiiBufferTarget::ElementArray:
        return GL_ELEMENT_ARRAY_BUFFER;

      case KawaiiBufferTarget::DispatchIndirect:
        return GL_DISPATCH_INDIRECT_BUFFER;

      case KawaiiBufferTarget::TextureBuffer:
        return GL_TEXTURE_BUFFER;

      case KawaiiBufferTarget::TransformFeedback:
        return GL_TRANSFORM_FEEDBACK_BUFFER;
      }
    Q_UNREACHABLE();
  }
}

void MisakaBufferHandle::bindWork(KawaiiBufferTarget target)
{
  GLenum glBufTarget = trBufferTarget(target);
  if(length)
    r.gl().glBindBuffer(glBufTarget, id);
}

void MisakaBufferHandle::unbindWork(KawaiiBufferTarget target)
{
  GLenum glBufTarget = trBufferTarget(target);
  r.gl().glBindBuffer(glBufTarget, 0);
}

void MisakaBufferHandle::bindToBlockWork(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  GLenum glBufTarget = trBufferTarget(target);
  if(length)
    r.gl().glBindBufferBase(glBufTarget, bindingPoint, id);
  if(uboPostbind)
    uboPostbind(glBufTarget, bindingPoint);
}

void MisakaBufferHandle::unbindFromBlockWork(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  GLenum glBufTarget = trBufferTarget(target);
  r.gl().glBindBufferBase(glBufTarget, bindingPoint, 0);
}
