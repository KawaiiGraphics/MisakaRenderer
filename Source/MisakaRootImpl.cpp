#include "MisakaRootImpl.hpp"
#include "MisakaBufferHandle.hpp"
#include "MisakaGpuBufImpl.hpp"
#include "Textures/MisakaTextureHandle.hpp"

#include <Kawaii3D/KawaiiConfig.hpp>

#include <QPainter>
#include <QCoreApplication>
#include <QOpenGLFunctions_4_5_Core>
#include <QOpenGLVersionFunctionsFactory>

#include <iostream>

MisakaRootImpl::MisakaRootImpl(KawaiiRoot *model):
  KawaiiRootImpl(model),
  glCtx(this),
  sfcStub(nullptr),
  glFuncs(nullptr),
  globTexturesBufBinded(false)
{
  moveToThread(QCoreApplication::instance()->thread());
  sfcStub.setParent(this);

  if(QThread::currentThread() == thread())
    initContext();
  else {
      bool ok = QMetaObject::invokeMethod(this, &MisakaRootImpl::initContext, Qt::BlockingQueuedConnection);
      Q_ASSERT(ok);
    }
}

MisakaRootImpl::~MisakaRootImpl()
{
  if(QThread::currentThread() == thread())
    deleteContext();
  else {
      if(QOpenGLContext::currentContext() == &glCtx)
        glCtx.doneCurrent();
      bool ok = QMetaObject::invokeMethod(this, &MisakaRootImpl::deleteContext, Qt::BlockingQueuedConnection);
      Q_ASSERT(ok);
    }
}

void MisakaRootImpl::taskGL(const std::function<void ()> &func)
{
  if(QThread::currentThread() == thread())
    {
      if(QOpenGLContext::currentContext() != &glCtx)
        glCtx.makeCurrent(&sfcStub);
      func();
    } else
    {
      bool ok = QMetaObject::invokeMethod(this, func, Qt::BlockingQueuedConnection);
      Q_ASSERT(ok);
    }
}

void MisakaRootImpl::asyncTaskGL(const std::function<void ()> &func)
{
  if(QThread::currentThread() == thread())
    {
      if(QOpenGLContext::currentContext() != &glCtx)
        glCtx.makeCurrent(&sfcStub);
      func();
    } else
    {
      bool ok = QMetaObject::invokeMethod(this, func, Qt::QueuedConnection);
      Q_ASSERT(ok);
    }
}

QSurfaceFormat MisakaRootImpl::requestedFormat()
{
  QSurfaceFormat fmt = KawaiiConfig::getInstance().getPreferredSfcFormat();
  fmt.setRenderableType(QSurfaceFormat::OpenGL);
  fmt.setProfile(QSurfaceFormat::CoreProfile);
  fmt.setVersion(4, 5);
  fmt.setDepthBufferSize(0);
  return fmt;
}

QOpenGLFunctions_4_5_Core &MisakaRootImpl::gl() const
{
  return *glFuncs;
}

QOpenGLExtension_ARB_bindless_texture& MisakaRootImpl::ARB_bindless_texture()
{
  return bindlessTexture;
}

MisakaBufferHandle &MisakaRootImpl::getGlobalTexturesUbo()
{
  if(!globalTexturesUbo)
    {
      globalTexturesUbo = std::make_unique<MisakaBufferHandle>(nullptr, 0, *this);
      globalTexturesUbo->setOnInvalidate([this] { globalTexturesUbo.reset(); } );
    }
  return *globalTexturesUbo;
}

void MisakaRootImpl::addOnShaderProgUsed(const std::function<void (GLint)> &func)
{
  onShaderProgUsed.push_back(func);
}

void MisakaRootImpl::useShaderProg(GLint prog)
{
  gl().glUseProgram(prog);
  if(!onShaderProgUsed.empty())
    {
      const auto _onShaderProgUsed = std::move(onShaderProgUsed);
      for(auto &func: _onShaderProgUsed)
        func(prog);
    }

  // todo: oprimize
  std::vector<GLuint> glTextures;
  glTextures.reserve(bindedInputGbufs.size() + bindedSampledGbufs.size());
  glTextures.insert(glTextures.end(), bindedInputGbufs.cbegin(), bindedInputGbufs.cend());
  glTextures.insert(glTextures.end(), bindedSampledGbufs.cbegin(), bindedSampledGbufs.cend());
  gl().glBindTextures(0, glTextures.size(), glTextures.data());

  for(size_t i = 0; i < bindedInputGbufs.size(); ++i)
    {
      const auto uniformName = QStringLiteral("INPUT_GBUF_%1").arg(i).toLatin1();
      const GLint uniformLocation = gl().glGetUniformLocation(prog, uniformName.data());
      if(uniformLocation > -1)
        gl().glUniform1i(uniformLocation, i);
    }
  for(size_t i = 0; i < bindedSampledGbufs.size(); ++i)
    {
      const auto uniformName = QStringLiteral("SAMPLED_GBUF_%1").arg(i).toLatin1();
      const GLint uniformLocation = gl().glGetUniformLocation(prog, uniformName.data());
      if(uniformLocation > -1)
        gl().glUniform1i(uniformLocation, i+bindedInputGbufs.size());
    }

  // todo: implement storage gbufs
  Q_ASSERT(bindedStorageGbufs.empty());

  if(globalTexturesUbo && globalTexturesUbo->getBufferSize() > 0 && !globTexturesBufBinded)
    {
      globalTexturesUbo->bindToBlock(KawaiiBufferTarget::UBO, 2*KawaiiShader::getUboCount()+1);
      globTexturesBufBinded = true;
    }
}

float MisakaRootImpl::getGlFloat(GLenum pname)
{
  auto el = glFloatParams.find(pname);
  if(el == glFloatParams.end())
    {
      float result[4] = {0, 0, 0, 0};
      taskGL([this, pname, &result] {
          gl().glGetFloatv(pname, result);
        });
      el = glFloatParams.insert({pname, result[0]}).first;
    }
  return el->second;
}

const QString& MisakaRootImpl::getShader5GlslExtension() const
{
  return glsl5Extension;
}

void MisakaRootImpl::makeCtxCurrent(QSurface *wnd)
{
  if(QOpenGLContext::currentContext() != &glCtx || glCtx.surface() != wnd)
    if(!glCtx.makeCurrent(wnd))
      qCritical("Can not make context current!");
}

void MisakaRootImpl::startRendering(QWindow *wnd)
{
  makeCtxCurrent(wnd);
  glFuncs->glBindFramebuffer(GL_FRAMEBUFFER, glCtx.defaultFramebufferObject());
  glFuncs->glDisable(GL_DEPTH_TEST);
  glFuncs->glClear(GL_COLOR_BUFFER_BIT);
}

void MisakaRootImpl::finishRendering(QWindow *wnd)
{
  glCtx.swapBuffers(wnd);
}

void MisakaRootImpl::setBindedUbo(GLenum target, GLuint bindingPoint, void *buf, KawaiiBufferTarget bufferTarget)
{
  bindedUbo[std::pair(target, bindingPoint)] = {buf, bufferTarget};
}

bool MisakaRootImpl::isUboBinded(GLenum target, GLuint bindingPoint, void *buf) const
{
  if(auto el = bindedUbo.find(std::pair(target, bindingPoint)); el != bindedUbo.end())
    return el->second.first == buf;
  else
    return false;
}

void MisakaRootImpl::glEnable(GLenum cap, bool enabled)
{
  if(enabled)
    gl().glEnable(cap);
  else
    gl().glDisable(cap);
}

QRhi &MisakaRootImpl::getQRhi()
{
  if(!qrhi && glCtx.isValid())
    {
      QRhiGles2NativeHandles handlers = {
        .context = &glCtx
      };
      QRhiGles2InitParams rhiInit;
      rhiInit.fallbackSurface = &sfcStub;
      rhiInit.format = glCtx.format();
      rhiInit.shareContext = nullptr;
      rhiInit.window = nullptr;

      qrhi.reset(QRhi::create(QRhi::OpenGLES2, &rhiInit, {}, &handlers));
    }
  return *qrhi;
}

void MisakaRootImpl::rebindGlobalTexturesUbo()
{
  glFuncs->glBindFramebuffer(GL_FRAMEBUFFER, glCtx.defaultFramebufferObject());
  for(const auto &i: bindedUbo)
    {
      static_cast<MisakaGpuBufImpl*>(i.second.first)->bindToBlock(i.second.second, i.first.second);
    }

  if(globalTexturesUbo && globalTexturesUbo->getBufferSize() > 0 && globTexturesBufBinded)
    globalTexturesUbo->bindToBlock(KawaiiBufferTarget::UBO, 2*KawaiiShader::getUboCount()+1);
}

void MisakaRootImpl::finalize(const QSize &sz)
{
  if(!qPaintDev)
    qPaintDev = std::make_unique<QOpenGLPaintDevice>(sz);
  else
    qPaintDev->setSize(sz);

  if(!qpainter)
    qpainter = std::make_unique<QPainter>(qPaintDev.get());
  else
    qpainter->begin(qPaintDev.get());
  qpainter->end();
}

namespace {
  template<size_t N>
  constexpr QLatin1String toLatin1(const char (&str)[N])
  { return QLatin1String(str, N); }
}

bool MisakaRootImpl::checkSystem()
{
  const bool dbg_enabled = KawaiiConfig::getInstance().getDebugLevel() > 0;
  auto dbg_prnt = [&dbg_enabled] (const auto &str) {
      if(dbg_enabled)
        std::cerr << str << std::endl;
    };

  auto fmt = requestedFormat();

  QOffscreenSurface sfc;
  sfc.setFormat(fmt);
  sfc.create();

  QOpenGLContext ctx;
  ctx.setFormat(fmt);

  if(!ctx.create() || !ctx.isValid())
    {
      dbg_prnt("MisakaRenderer failed to init OpenGL 4.5 Core context!");
      return false;
    }

  if(!ctx.makeCurrent(&sfc))
    {
      dbg_prnt("MisakaRenderer failed to make OpenGL 4.5 Core context current!");
      return false;
    }

  bool result = true;

  QOpenGLFunctions_4_5_Core *gl_func = QOpenGLVersionFunctionsFactory::get<QOpenGLFunctions_4_5_Core>(&ctx);
  if(!gl_func)
    {
      dbg_prnt("MisakaRenderer failed to init OpenGL 4.5 Core functions!");
      result = false;
    }

  if(!ctx.extensions().contains("GL_ARB_bindless_texture"))
    {
      dbg_prnt("MisakaRenderer failed to init extension GL_ARB_bindless_texture");
      result = false;
    }

//  if(!ctx.supportsThreadedOpenGL())
//    {
//      dbg_prnt("MisakaRenderer failed to use threaded OpenGL!");
//      result = false;
//    }

  ctx.doneCurrent();
  sfc.destroy();

  return result;
}

KawaiiBufferHandle *MisakaRootImpl::createBuffer(const void *ptr, size_t n, const std::initializer_list<KawaiiBufferTarget> &availableTargets)
{
  static_cast<void>(availableTargets);
  return new MisakaBufferHandle(ptr, n, *this);
}

KawaiiTextureHandle *MisakaRootImpl::createTexture(KawaiiTexture *model)
{
  return new MisakaTextureHandle(*this, model);
}

void MisakaRootImpl::beginOffscreen()
{ }

void MisakaRootImpl::endOffscreen()
{
  glFuncs->glBindFramebuffer(GL_FRAMEBUFFER, glCtx.defaultFramebufferObject());
}

void MisakaRootImpl::setBindedStorageGbufs(const std::vector<GLuint> &newBindedStorageGbufs)
{
  bindedStorageGbufs = newBindedStorageGbufs;
}

void MisakaRootImpl::setBindedSampledGbufs(const std::vector<GLuint> &newBindedSampledGbufs)
{
  bindedSampledGbufs = newBindedSampledGbufs;
}

void MisakaRootImpl::setBindedInputGbufs(const std::vector<GLuint> &newBindedInputGbufs)
{
  bindedInputGbufs = newBindedInputGbufs;
}

void MisakaRootImpl::clearBindedGBufs()
{
  bindedInputGbufs.clear();
  bindedSampledGbufs.clear();
  bindedStorageGbufs.clear();
}

QOpenGLShaderProgram &MisakaRootImpl::getArrayedOverlayShaders(uint32_t layersCount)
{
  static const QByteArray vertSrc =
      "#version 450 core\n"

      "const vec2 vertices[6] = vec2[6] (\n"
      "  vec2(1,-1), vec2(1,1), vec2(-1,1),\n"
      "  vec2(-1,1), vec2(-1,-1), vec2(1,-1)\n"
      ");\n"

      "const vec2 coords[6] = vec2[6] (\n"
      "    vec2(1,1), vec2(1,0), vec2(0,0),\n"
      "    vec2(0,0), vec2(0,1), vec2(1,1)\n"
      ");\n"

      "layout (location = 0) uniform float aspectRatio = 1.0;" // (tex.height * screen.width) / (tex.width * screen.height)\n"

      "layout (location = 0) out vec2 texcoord;\n"

      "void main(void)\n"
      "{\n"
      "    texcoord = coords[gl_VertexID];\n"
      "    gl_Position = vec4(vertices[gl_VertexID], -1.0, 1.0);\n"
      "    if(aspectRatio > 1)\n"
      "        gl_Position.x /= aspectRatio;\n"
      "    else if(aspectRatio < 1)\n"
      "        gl_Position.y *= aspectRatio;\n"
      "}\n";

  if(layersCount == 1)
    {
      if(!quadShader)
        {
          static const QByteArray fragSrc = "#version 450 core\n#extension GL_ARB_bindless_texture: require\n"
              "layout (location = 1, bindless_sampler) uniform sampler2D inputTex;\n"
              "layout (location = 0) in vec2 texcoord;\n"
              "layout (location = 0) out vec4 outColor;\n"
              "void main(void)"
              "{"
              "    outColor = texture(inputTex, texcoord);\n"
              "}";
          quadShader = std::make_unique<QOpenGLShaderProgram>();
          quadShader->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex, vertSrc);
          quadShader->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment, fragSrc);
          quadShader->link();
        }
      return *quadShader;
    } else
    {
      auto &shader = quadArrayedShaders[layersCount];
      if(!shader)
        {
          QByteArray fragSrc = "#version 450 core\n#extension GL_ARB_bindless_texture: require\n"
                            "layout (location = 1, bindless_sampler) uniform sampler2DArray inputTex;\n"
                            "layout (location = 0) in vec2 texcoord;\n";

          for(uint32_t i = 0; i < layersCount; ++i)
            fragSrc += QStringLiteral("layout (location = %1) out vec4 outColor_%1;\n").arg(i).toLatin1();

          fragSrc += "void main (void) {\n";
          for(uint32_t i = 0; i < layersCount; ++i)
            fragSrc += QStringLiteral("    outColor_%1 = texture(inputTex, vec3(texcoord, %1));\n").arg(i).toLatin1();
          fragSrc += "}\n";

          shader = std::make_unique<QOpenGLShaderProgram>();
          shader->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex, vertSrc);
          shader->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment, fragSrc);
          shader->link();
        }
      return *shader;
    }
}

void MisakaRootImpl::drawOverlayQuad()
{
  if(!quadVao)
    {
      quadVao = std::make_unique<QOpenGLVertexArrayObject>();
      quadVao->create();
    }
  quadVao->bind();
  gl().glDrawArrays(GL_TRIANGLES, 0, 6);
  quadVao->release();
}

namespace
{
  template<size_t N>
  constexpr QLatin1String latin1Str(const char (&str)[N])
  {
    return str[N-1] == '\0'? QLatin1String(str, N-1): QLatin1String(str, N);
  }
}

void MisakaRootImpl::receiveGLDebug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *)
{
  const uint16_t dbg = KawaiiConfig::getInstance().getDebugLevel();

  static const std::unordered_map<GLenum, QLatin1String> sourceNames = {
    {GL_DEBUG_SOURCE_API,             latin1Str("calls to the OpenGL API")},
    {GL_DEBUG_SOURCE_WINDOW_SYSTEM,   latin1Str("calls to a window-system API")},
    {GL_DEBUG_SOURCE_SHADER_COMPILER, latin1Str("compiler for a shading language")},
    {GL_DEBUG_SOURCE_THIRD_PARTY,     latin1Str("application associated with OpenGL")},
    {GL_DEBUG_SOURCE_APPLICATION,     latin1Str("user of this application")},
    {GL_DEBUG_SOURCE_OTHER,           latin1Str("Unknown")}
  };

  static const std::unordered_map<GLenum, QLatin1String> typeNames = {
    {GL_DEBUG_TYPE_ERROR,               latin1Str("Error")},
    {GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, latin1Str("Warning (deprecated)")},
    {GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR,  latin1Str("Warning (undefined behavior)")},
    {GL_DEBUG_TYPE_PORTABILITY,         latin1Str("Warning (functionality is not portable)")},
    {GL_DEBUG_TYPE_PERFORMANCE,         latin1Str("Warning (performance issues)")},
    {GL_DEBUG_TYPE_MARKER,              latin1Str("Command stream annotation")},
    {GL_DEBUG_TYPE_PUSH_GROUP,          latin1Str("Group pushed")},
    {GL_DEBUG_TYPE_POP_GROUP,           latin1Str("Group poped")},
    {GL_DEBUG_TYPE_OTHER,               latin1Str("Unknown")}
  };

  if(length < 0 || strlen(reinterpret_cast<const char*>(message)) < static_cast<size_t>(length))
    return;

  switch(severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:
      qCritical().setAutoInsertSpaces(true);
      qCritical() << "OpenGL debug message #" << id << '{';
      qCritical() << "\tFrom " << sourceNames.at(source);
      qCritical() << "\tType: " << typeNames.at(type);
      qCritical() << "\tText: " << reinterpret_cast<const char*>(message);
      qCritical().nospace() << '}';
      break;

    case GL_DEBUG_SEVERITY_MEDIUM:
      if(dbg < 2) break;
      qWarning() << "OpenGL debug message #" << id << '{';
      qWarning() << "\tFrom " << sourceNames.at(source);
      qWarning() << "\tType: " << typeNames.at(type);
      qWarning() << "\tText: " << reinterpret_cast<const char*>(message);
      qWarning().nospace() << '}';
      break;

    default:
      if(dbg < 3) break;
      qInfo() << "OpenGL debug message #" << id << '{';
      qInfo() << "\tFrom " << sourceNames.at(source);
      qInfo() << "\tType: " << typeNames.at(type);
      qInfo() << "\tText: " << reinterpret_cast<const char*>(message);
      qInfo().nospace() << '}';
      break;
    }
}

void MisakaRootImpl::initContext()
{
  const QSurfaceFormat fmt = requestedFormat();

  glCtx.setFormat(fmt);
  glCtx.create();

  sfcStub.setFormat(fmt);
  sfcStub.create();
  glCtx.makeCurrent(&sfcStub);

  glFuncs = QOpenGLVersionFunctionsFactory::get<QOpenGLFunctions_4_5_Core>(&glCtx);
  glFuncs->glEnable(GL_DEPTH_TEST);
  glFuncs->glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
  if(KawaiiConfig::getInstance().getDebugLevel())
    {
      glFuncs->glEnable(GL_DEBUG_OUTPUT);
      glFuncs->glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
      glFuncs->glDebugMessageCallback(&MisakaRootImpl::receiveGLDebug, nullptr);
    }

  glFuncs->glClearColor(0,0,0, 1.0);

  bindlessTexture.initializeOpenGLFunctions();

  const auto extList = glCtx.extensions();
  if(extList.contains("GL_NV_gpu_shader5"))
    glsl5Extension = "#extension GL_NV_gpu_shader5: enable\n";
  else if(extList.contains("GL_ARB_gpu_shader5"))
    glsl5Extension = "#extension GL_ARB_gpu_shader5: enable\n";
  else
    glsl5Extension.clear();
}

void MisakaRootImpl::deleteContext()
{
  globalTexturesUbo.reset();
  quadShader.reset();
  quadArrayedShaders.clear();
  if(quadVao)
    {
      quadVao->destroy();
      quadVao.reset();
    }
  qrhi.reset();
  glCtx.doneCurrent();
  sfcStub.destroy();
}
