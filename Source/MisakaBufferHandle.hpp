#ifndef MISAKABUFFERHANDLE_HPP
#define MISAKABUFFERHANDLE_HPP

#include <KawaiiRenderer/KawaiiBufferHandle.hpp>
#include <QOpenGLFunctions_4_5_Core>
#include "MisakaRenderer_global.hpp"
#include <sib_utils/PairHash.hpp>
#include <functional>

class MisakaRootImpl;

extern template class sib_utils::PairHash<GLenum, GLuint>;

class MISAKARENDERER_SHARED_EXPORT MisakaBufferHandle: public KawaiiBufferHandle
{
public:
  MisakaBufferHandle(const void *ptr, size_t n, MisakaRootImpl &root);
  ~MisakaBufferHandle();

  void setUboPostbindFunc(const std::function<void(GLenum, uint32_t)> &func);

  void bindVbo(GLuint vao_id, GLuint bindingIndex, GLintptr offset, GLsizei stride) const;
  void bindElementBuffer(GLuint vao_id) const;

  void setSubData(size_t offset, const void *ptr, size_t n);

  // KawaiiBufferHandle interface
  void setBufferData(const void *ptr, size_t n) override final;
  void sendChunkToGpu(size_t offset, size_t n) override final;
  size_t getBufferSize() const override final;
  void bind(KawaiiBufferTarget target) override final;
  void unbind(KawaiiBufferTarget target) override final;
  void bindToBlock(KawaiiBufferTarget target, uint32_t bindingPoint) override final;
  void userBinding(KawaiiBufferTarget target, uint32_t bindingPoint) override final;
  void unbindFromBlock(KawaiiBufferTarget target, uint32_t bindingPoint) override final;



  //IMPLEMENT
private:
  std::function<void(GLenum, uint32_t)> uboPostbind;

  MisakaRootImpl &r;
  const void *data;
  size_t length;
  GLuint id;

  void setBufferDataWork(const void *ptr, size_t n);
  void setSubDataWork(size_t offset, const void *ptr, size_t n);
  void bindWork(KawaiiBufferTarget target);
  void unbindWork(KawaiiBufferTarget target);
  void bindToBlockWork(KawaiiBufferTarget target, uint32_t bindingPoint);
  void unbindFromBlockWork(KawaiiBufferTarget target, uint32_t bindingPoint);
};

#endif // MISAKABUFFERHANDLE_HPP
