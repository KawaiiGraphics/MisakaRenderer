#ifndef MISAKASHADERIMPL_HPP
#define MISAKASHADERIMPL_HPP

#include <KawaiiRenderer/Shaders/KawaiiShaderImpl.hpp>
#include "../MisakaRenderer_global.hpp"
#include <qopengl.h>
#include <QSet>

class MisakaRootImpl;
class MISAKARENDERER_SHARED_EXPORT MisakaShaderImpl : public KawaiiShaderImpl
{
public:
  MisakaShaderImpl(KawaiiShader *model);
  ~MisakaShaderImpl();

  void useInProgram(GLuint program);

  const std::string& getGlslCode() const;
  QString getModuleFName() const;

  // KawaiiShaderImpl interface
private:
  void createShader() override final;
  void deleteShader() override final;
  void recompile() override final;



  //IMPLEMENT
private:
  KawaiiShader *modelObj;
  std::string glsl_src;
  QSet<QString> injectedTextures;

  GLuint id;

  bool isTextureInjected(const QString &texture);

  void preprocessGlsl(const QString &code);
  MisakaRootImpl* rootImpl();

  void createShaderWork();
  void deleteShaderWork();
  void recompileWork();

  void printLog(GLint glsl_length);
};

#endif // MISAKASHADERIMPL_HPP
