#include "MisakaProgramImpl.hpp"
#include "MisakaShaderImpl.hpp"
#include "MisakaRootImpl.hpp"
#include <Kawaii3D/KawaiiConfig.hpp>

MisakaProgramImpl::MisakaProgramImpl(KawaiiProgram *model):
  KawaiiProgramImpl(model),
  id(0),
  linked(false)
{
}

MisakaProgramImpl::~MisakaProgramImpl()
{
  preDestruct();
}

bool MisakaProgramImpl::use() const
{
  if(linked)
    {
      rootImpl()->useShaderProg(id);
      GLenum cullMode = GL_BACK;
      switch(static_cast<KawaiiProgram*>(getData())->getCullMode())
        {
        case KawaiiProgram::CullMode::CullNone:
          rootImpl()->gl().glDisable(GL_CULL_FACE);
          cullMode = GL_FRONT_AND_BACK;
          break;
        case KawaiiProgram::CullMode::CullBack:
          rootImpl()->gl().glEnable(GL_CULL_FACE);
          cullMode = GL_BACK;
          break;
        case KawaiiProgram::CullMode::CullFront:
          rootImpl()->gl().glEnable(GL_CULL_FACE);
          cullMode = GL_FRONT;
          break;
        case KawaiiProgram::CullMode::CullAll:
          rootImpl()->gl().glEnable(GL_CULL_FACE);
          cullMode = GL_FRONT_AND_BACK;
          break;
        }
      rootImpl()->gl().glCullFace(cullMode);
    }
  return linked;
}

void MisakaProgramImpl::createProgram()
{
  rootImpl()->taskGL(std::bind(&MisakaProgramImpl::createProgramWork, this));
}

void MisakaProgramImpl::deleteProgram()
{
  rootImpl()->taskGL(std::bind(&MisakaProgramImpl::deleteProgramWork, this));
}

void MisakaProgramImpl::reAddShader(KawaiiShaderImpl *shader)
{
  rootImpl()->asyncTaskGL(std::bind(&MisakaProgramImpl::reAddShaderWork, this, shader));
}

MisakaRootImpl *MisakaProgramImpl::rootImpl() const
{
  return static_cast<MisakaRootImpl*>(getRoot());
}

void MisakaProgramImpl::createProgramWork()
{
  auto &gl = rootImpl()->gl();
  id = gl.glCreateProgram();
  forallShaders([this] (KawaiiShaderImpl *shader) {
      static_cast<MisakaShaderImpl*>(shader)->useInProgram(id);
    });
  bool hasVertexShader = false;
  static_cast<KawaiiProgram*>(getData())->forallModules([&hasVertexShader](KawaiiShader *sh) {
    if(sh->getType() == KawaiiShaderType::Vertex)
      hasVertexShader = true;
  });
  if(!hasVertexShader)
    {
      if(!quadVertexShader)
        {
          quadVertexShader = std::make_unique<QOpenGLShader>(QOpenGLShader::Vertex);
          quadVertexShader->compileSourceCode("#version 450 core\n"
                                              "const vec2 vertices[6] = vec2[6] (\n"
                                              "  vec2(1,-1), vec2(1,1), vec2(-1,1),\n"
                                              "  vec2(-1,1), vec2(-1,-1), vec2(1,-1)\n"
                                              ");\n"
                                              "void main(void) {\n"
                                              "  gl_Position = vec4(vertices[gl_VertexID], -1.0, 1.0);\n"
                                              "}\n");
        }
      gl.glAttachShader(id, quadVertexShader->shaderId());
    }
  gl.glLinkProgram(id);
  printLog();
}

void MisakaProgramImpl::deleteProgramWork()
{
  rootImpl()->gl().glDeleteProgramPipelines(1, &id);
}

void MisakaProgramImpl::reAddShaderWork([[maybe_unused]] KawaiiShaderImpl *shader)
{
//  static_cast<MisakaShaderImpl*>(shader)->useInProgram(id);
  rootImpl()->gl().glLinkProgram(id);
  printLog();
}

namespace {
  const bool dumpingShaders = !qgetenv("SIB3D_DUMP_SHADERS").isEmpty()
      && qgetenv("SIB3D_DUMP_SHADERS").trimmed() != "0";
}

void MisakaProgramImpl::printLog()
{
  if(dumpingShaders)
    dumpShaders();

  GLint success;
  rootImpl()->gl().glGetProgramiv(id, GL_LINK_STATUS, &success);
  linked = (success != 0);
  if(!success && KawaiiConfig::getInstance().getDebugLevel() > 0)
    {
      GLint logSize;
      rootImpl()->gl().glGetProgramiv(id, GL_INFO_LOG_LENGTH, &logSize);
      std::vector<char> linkLog(logSize+1, '\0');
      GLsizei realLogSize;
      rootImpl()->gl().glGetProgramInfoLog(id, logSize, &realLogSize, &linkLog[0]);
      QLatin1String linkLogStr(&linkLog[0], realLogSize);

      qCritical().noquote() << objectName() << " (MisakaProgram): Linking failed!\n" << linkLogStr;
      qCritical().quote();
    }
}

QString MisakaProgramImpl::getName() const
{
  const QString dataObjName = getData()->objectName();
  if(!dataObjName.isNull() && !dataObjName.isEmpty())
    return dataObjName;
  else
    return QStringLiteral("0x%1").arg(reinterpret_cast<qulonglong>(getData()), 0, 16);
}

void MisakaProgramImpl::dumpShaders() const
{
  QDir d = QDir::temp();
  const auto path = QStringLiteral("MisakaRenderer/%1_shaders/").arg(getName());
  d.mkpath(path);
  d.cd(path);
  forallShaders([&d](KawaiiShaderImpl *shader) {
    auto *sh = static_cast<MisakaShaderImpl*>(shader);
    QFile f(d.absoluteFilePath(sh->getModuleFName()));
    f.open(QFile::WriteOnly | QFile::Text);
    f.write(QByteArray::fromRawData(sh->getGlslCode().data(), sh->getGlslCode().size()));
    f.close();
  });
}

