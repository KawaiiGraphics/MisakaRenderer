#include "MisakaShaderImpl.hpp"
#include "MisakaRootImpl.hpp"

#include <sib_utils/Strings.hpp>
#include <Kawaii3D/KawaiiConfig.hpp>

#include <QRegularExpression>

#include <array>

namespace {
  GLenum trShaderType(KawaiiShaderType type)
  {
    switch(type)
      {
      case KawaiiShaderType::Vertex:
        return GL_VERTEX_SHADER;
      case KawaiiShaderType::Fragment:
        return GL_FRAGMENT_SHADER;
      case KawaiiShaderType::Compute:
        return GL_COMPUTE_SHADER;
      case KawaiiShaderType::Geometry:
        return GL_GEOMETRY_SHADER;
      case KawaiiShaderType::TesselationControll:
        return GL_TESS_CONTROL_SHADER;
      case KawaiiShaderType::TesselationEvaluion:
        return GL_TESS_EVALUATION_SHADER;
      }
    Q_UNREACHABLE();
  }
}

MisakaShaderImpl::MisakaShaderImpl(KawaiiShader *model):
  KawaiiShaderImpl(model),
  modelObj(model),
  id(0)
{
  initResource();
}

MisakaShaderImpl::~MisakaShaderImpl()
{
  preDestruct();
}

void MisakaShaderImpl::useInProgram(GLuint program)
{
  if(id && program)
    rootImpl()->gl().glAttachShader(program, id);
}

const std::string &MisakaShaderImpl::getGlslCode() const
{
  return glsl_src;
}

QString MisakaShaderImpl::getModuleFName() const
{
  auto result = modelObj->getModuleName();
  switch(modelObj->getType())
    {
    case KawaiiShaderType::Fragment:
      result += QStringLiteral(".frag.glsl");
      break;
    case KawaiiShaderType::TesselationControll:
      result += QStringLiteral(".tesc.glsl");
      break;
    case KawaiiShaderType::TesselationEvaluion:
      result += QStringLiteral(".tese.glsl");
      break;
    case KawaiiShaderType::Vertex:
      result += QStringLiteral(".vert.glsl");
      break;
    case KawaiiShaderType::Geometry:
      result += QStringLiteral(".geom.glsl");
      break;
    case KawaiiShaderType::Compute:
      result += QStringLiteral(".comp.glsl");
      break;
    default: Q_UNREACHABLE();
    }
  return result;
}

void MisakaShaderImpl::createShader()
{
  rootImpl()->taskGL(std::bind(&MisakaShaderImpl::createShaderWork, this));
}

void MisakaShaderImpl::deleteShader()
{
  rootImpl()->taskGL(std::bind(&MisakaShaderImpl::deleteShaderWork, this));
}

void MisakaShaderImpl::recompile()
{
  if(!id)
    return createShader();

  rootImpl()->asyncTaskGL(std::bind(&MisakaShaderImpl::recompileWork, this));
}

bool MisakaShaderImpl::isTextureInjected(const QString &texture)
{
  for(auto *sh: modelObj->getOwner()->getShaders(modelObj->getType()))
    {
      auto shImpl = static_cast<MisakaShaderImpl*>(sh->getRendererImpl());
      if(Q_LIKELY(shImpl) && shImpl->injectedTextures.contains(texture))
        return true;
    }

  return false;
}

namespace {
  static const QString storageImgGlslPattern = QStringLiteral("layout (%1) uniform image2D STORAGE_GBUF_%2;");
  static const QString readonlyImgGlslPattern = QStringLiteral("layout (%1) readonly uniform image2D STORAGE_GBUF_%2;");
  static const QString sampledImgGlslPattern = QStringLiteral("uniform sampler2D SAMPLED_GBUF_%1;");
  static const QString inputImgGlslPattern = QStringLiteral("uniform sampler2D INPUT_GBUF_%1;");
}

void MisakaShaderImpl::preprocessGlsl(const QString &code)
{
  injectedTextures.clear();
  QString glsl = code;

  std::array<int, 4> blockEnds = {
    modelObj->getCameraBlockEnd(),
    modelObj->getSurfaceBlockEnd(),
    modelObj->getMaterialBlockEnd(),
    modelObj->getModelBlockEnd()
  };
  struct ReplacementDefinition {
    QString after;
    int b;
    int sz;
  };

  std::map<int, ReplacementDefinition> replacements;
  bool hasInputGBufs = false;
  modelObj->forallInputGbuf([&replacements, &hasInputGBufs] (uint32_t gbufIndex, const KawaiiShader::GBufDefinition &gbuf) {
      const QString after = inputImgGlslPattern.arg(gbufIndex);
      replacements[gbuf.i0] = ReplacementDefinition { .after = after, .b = gbuf.i0, .sz = gbuf.sz };
      hasInputGBufs = true;
    });
  modelObj->forallStorageGbuf([&replacements] (uint32_t gbufIndex, const KawaiiShader::GBufDefinition &gbuf) {
      const QString after = (gbuf.readonly? readonlyImgGlslPattern: storageImgGlslPattern)
          .arg(gbuf.format)
          .arg(gbufIndex);

      replacements[gbuf.i0] = ReplacementDefinition { .after = after, .b = gbuf.i0, .sz = gbuf.sz };
    });
  modelObj->forallSampledGbuf([&replacements] (uint32_t gbufIndex, const KawaiiShader::GBufDefinition &gbuf) {
      const QString after = sampledImgGlslPattern.arg(gbufIndex);
      replacements[gbuf.i0] = ReplacementDefinition { .after = after, .b = gbuf.i0, .sz = gbuf.sz };
    });
  for(auto i = replacements.rbegin(); i != replacements.rend(); ++i)
    {
      glsl.replace(i->second.b,i->second.sz, i->second.after);
      int delta = i->second.after.size() - (i->second.sz);
      for(int &el: blockEnds)
        if(el > i->second.b)
          el += delta;
    }

  std::map<int, QString> injections;
  if(hasInputGBufs)
    injections[0] = QStringLiteral("vec4 subpassLoad(in sampler2D gbuf) { return texelFetch(gbuf, ivec2(gl_FragCoord.x, gl_FragCoord.y), 0); }\n");

  auto injectTextureHandles = [this,&injections](int blockEnd, const QString &blockName, int blockIndex, const QHash<QString, QString> &textures) {
      if(textures.isEmpty())
        return;
      QString block = QStringLiteral("\nlayout(shared, binding = %1) uniform %2\n{\n").arg(KawaiiShader::getUboCount() + blockIndex).arg(blockName),
          global;

      static const QString globTerm = QStringLiteral("%2 %1 = %2(%3);\n");
      static const QString globTermReuse = QStringLiteral("%2 %1;\n");

      for(auto i = textures.begin(); i != textures.end(); ++i)
        {
          QString handleName = i.key() + "_handle";
          block += "    uvec2 " + handleName + ";\n";
          auto components = i->split(' ');
          if(!isTextureInjected(i.key()))
            {
              global += globTerm.arg(components.last(), components.first(), handleName);
              injectedTextures.insert(i.key());
            } else
            global += globTermReuse.arg(components.last(), components.first());
        }
      block += "};\n" + global;
      injections[blockEnd+1] += block;
    };

  injectTextureHandles(blockEnds[0], QStringLiteral("SIB_CAMERA_TEXTURES"), KawaiiShader::getCameraUboLocation(), modelObj->getCameraTextures());
  injectTextureHandles(blockEnds[1], QStringLiteral("SIB_SURFACE_TEXTURES"), KawaiiShader::getSurfaceUboLocation(), modelObj->getSurfaceTextures());
  injectTextureHandles(blockEnds[2], QStringLiteral("SIB_MATERIAL_TEXTURES"), KawaiiShader::getMaterialUboLocation(), modelObj->getMaterialTextures());
  injectTextureHandles(blockEnds[3], QStringLiteral("SIB_MODELS_TEXTURES"), KawaiiShader::getModelUboLocation(), modelObj->getModelTextures());
  injectTextureHandles(-1,           QStringLiteral("SIB_GLOBAL_TEXTURES"), KawaiiShader::getUboCount() + 1, modelObj->getGlobalTextures());

  for(auto i = injections.rbegin(); i != injections.rend(); ++i)
    glsl.insert(i->first, i->second);

  static QRegularExpression regEx(QStringLiteral("#version\\s+\\d+\\s+core\\s*"));
  auto versionMatch = regEx.match(glsl);
  if(versionMatch.hasMatch())
    {
      if(!injections.empty())
        {
          int i = versionMatch.capturedEnd();
          glsl.insert(i, "\n#extension GL_ARB_bindless_texture: require\n"
                      + rootImpl()->getShader5GlslExtension());
        }
    } else
    {
      const QString prefix = injections.empty()?
            "#version 450 core\n" + rootImpl()->getShader5GlslExtension():
            "#version 450 core\n#extension GL_ARB_bindless_texture: require\n"
            + rootImpl()->getShader5GlslExtension();
      glsl = prefix + glsl;
    }

  static QRegularExpression globalUboExp("GLOBAL_UBO_BINDING_(\\d+)");
  glsl.replace(globalUboExp, QString("(\\1 + %1)").arg(2*KawaiiShader::getUboCount() + 1));

  static QRegularExpression globalSSBOExp("GLOBAL_SSBO_BINDING_(\\d+)");
  glsl.replace(globalSSBOExp, QStringLiteral("\\1"));

  auto terms = glsl.split(';', Qt::SkipEmptyParts);
  for(auto i = terms.begin(); i != terms.end(); )
    {
      if(Q_UNLIKELY(i->trimmed().isEmpty()))
        i = terms.erase(i);
      else
        ++i;
    }
  glsl = terms.join(';');

  glsl_src = glsl.toStdString();
}

MisakaRootImpl *MisakaShaderImpl::rootImpl()
{
  return static_cast<MisakaRootImpl*>(getRoot());
}

void MisakaShaderImpl::createShaderWork()
{
  Q_ASSERT(id == 0);

  preprocessGlsl(modelObj->getCode());
  const GLchar *gl_str[1] = { reinterpret_cast<const GLchar*>(glsl_src.c_str()) };
  const GLint str_length[1] = { static_cast<GLint>(glsl_src.length()) };

  auto &gl = rootImpl()->gl();
  id = gl.glCreateShader(trShaderType(modelObj->getType()));
  gl.glShaderSource(id, 1, gl_str, str_length);
  gl.glCompileShader(id);

  printLog(str_length[0]);
}

void MisakaShaderImpl::deleteShaderWork()
{
  if(id)
    {
      auto &gl = rootImpl()->gl();
      gl.glDeleteShader(id);
      id=0;
    }
}

void MisakaShaderImpl::recompileWork()
{
  if(!id)
    return createShaderWork();

  preprocessGlsl(modelObj->getCode());
  const GLchar *gl_str[1] = { reinterpret_cast<const GLchar*>(glsl_src.c_str()) };
  const GLint str_length[1] = { static_cast<GLint>(glsl_src.length()) };

  auto &gl = rootImpl()->gl();
  gl.glShaderSource(id, 1, gl_str, str_length);
  gl.glCompileShader(id);

  printLog(str_length[0]);
}

void MisakaShaderImpl::printLog(GLint glsl_length)
{
  auto &gl = rootImpl()->gl();

  if(KawaiiConfig::getInstance().getDebugLevel() > 0)
    {
      int infoLogLength;
      gl.glGetShaderiv(id, GL_INFO_LOG_LENGTH, &infoLogLength);
      if ( infoLogLength > 0 )
        {
          GLint compiled;
          gl.glGetShaderiv(id, GL_COMPILE_STATUS, &compiled);

          if(!compiled)
            {
              std::vector<char> msg(infoLogLength+1),
                  src(glsl_length);

              gl.glGetShaderInfoLog(id, infoLogLength, nullptr, &msg[0]);
              gl.glGetShaderSource(id, glsl_length, nullptr, &src[0]);

              qCritical() << objectName() << " (MisakaShader): compilation failed!";

              QLatin1String srcStr(&src[0], src.size()),
                  msgStr(&msg[0], msg.size());

              qCritical() << "Problematic shader code:";
              qCritical().noquote() << srcStr;
              qCritical() << "::::::\r\nShader compile log:";
              qCritical().noquote() << msgStr;
              qCritical().quote();
            }
        }
    }
}
