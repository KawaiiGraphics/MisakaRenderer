#ifndef MISAKAPROGRAMIMPL_HPP
#define MISAKAPROGRAMIMPL_HPP

#include <KawaiiRenderer/Shaders/KawaiiProgramImpl.hpp>
#include "../MisakaRenderer_global.hpp"
#include <QOpenGLShader>
#include <qopengl.h>

class MisakaRootImpl;
class MisakaShaderImpl;
class MISAKARENDERER_SHARED_EXPORT MisakaProgramImpl : public KawaiiProgramImpl
{
public:
  MisakaProgramImpl(KawaiiProgram *model);
  ~MisakaProgramImpl();

  // KawaiiProgramImpl interface
public:
  bool use() const override final;

private:
  void createProgram() override final;
  void deleteProgram() override final;
  void reAddShader(KawaiiShaderImpl *shader) override final;



  //IMPLEMENT
private:
  std::unique_ptr<QOpenGLShader> quadVertexShader;
  GLuint id;
  bool linked;

  MisakaRootImpl* rootImpl() const;

  void createProgramWork();
  void deleteProgramWork();
  void reAddShaderWork(KawaiiShaderImpl *shader);

  void printLog();

  QString getName() const;
  void dumpShaders() const;
};

#endif // MISAKAPROGRAMIMPL_HPP
