#ifndef MISAKASURFACEIMPL_HPP
#define MISAKASURFACEIMPL_HPP

#include <KawaiiRenderer/Surfaces/KawaiiSurfaceImpl.hpp>
#include "../MisakaRenderer_global.hpp"

#include <sib_utils/PairHash.hpp>
#include <QOpenGLPaintDevice>

class MisakaRootImpl;
class MISAKARENDERER_SHARED_EXPORT MisakaSurfaceImpl : public KawaiiSurfaceImpl
{
public:
  MisakaSurfaceImpl(KawaiiSurface *model);

private:
  // KawaiiSurfaceImpl interface
  void resize(const QSize &sz) override final;

  bool startRendering() override final;
  void finishRendering() override final;

  inline void invalidate() override final {}
};

#endif // MISAKASURFACEIMPL_HPP
