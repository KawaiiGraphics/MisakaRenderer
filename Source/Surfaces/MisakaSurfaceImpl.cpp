#include "MisakaSurfaceImpl.hpp"
#include "MisakaRootImpl.hpp"
#include "Renderpass/MisakaRenderpassImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include <QOpenGLPaintDevice>

MisakaSurfaceImpl::MisakaSurfaceImpl(KawaiiSurface *model):
  KawaiiSurfaceImpl(model)
{
  const auto wndStates = getModel()->getWindow().windowStates();
  bool wasVisible = getModel()->getWindow().isVisible();

  getModel()->getWindow().setSurfaceType(QSurface::OpenGLSurface);
  getModel()->getWindow().setFormat(MisakaRootImpl::requestedFormat());

  getModel()->getWindow().destroy();
  getModel()->getWindow().create();
  getModel()->getWindow().setWindowStates(wndStates);
  getModel()->getWindow().setVisible(wasVisible);
}

void MisakaSurfaceImpl::resize(const QSize &sz)
{
  static_cast<void>(sz);
}

bool MisakaSurfaceImpl::startRendering()
{
  auto root = static_cast<MisakaRootImpl*>(getModel()->getRoot()->getRendererImpl());
  for(size_t i = 0; i < getModel()->tileCount(); ++i)
    if(getModel()->getTile(i).getRenderpass())
      {
        root->startRendering(&getModel()->getWindow());
        return true;
      }
  return false;
}

void MisakaSurfaceImpl::finishRendering()
{
  for(size_t i = 0; i < getModel()->tileCount(); ++i)
    {
      const auto &tile = getModel()->getTile(i);
      auto rp = tile.getRenderpass();
      if(rp)
        static_cast<MisakaRenderpassImpl*>(rp->getRendererImpl())->blitSfcColor
          (tile.getResultRect().x(), tile.getResultRect().y(), tile.getResultRect().width(), tile.getResultRect().height());
    }
  static_cast<MisakaRootImpl*>(getModel()->getRoot()->getRendererImpl())->finishRendering(&getModel()->getWindow());
  getModel()->getWindow().requestUpdate();
}
