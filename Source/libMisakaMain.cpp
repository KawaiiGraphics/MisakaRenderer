#include "KawaiiMisakaFactory.hpp"
#include "MisakaRootImpl.hpp"
#include <QWindow>

extern "C" MISAKARENDERER_SHARED_EXPORT KawaiiRendererImpl* createMisakaRoot(KawaiiRoot *root)
{
  return KawaiiMisakaFactory::getInstance()->createRenderer(*root);
}

extern "C" MISAKARENDERER_SHARED_EXPORT bool checkMisaka()
{
  return MisakaRootImpl::checkSystem();
}
