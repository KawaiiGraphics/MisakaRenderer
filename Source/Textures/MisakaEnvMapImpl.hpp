#ifndef MISAKAENVMAPIMPL_HPP
#define MISAKAENVMAPIMPL_HPP

#include <KawaiiRenderer/Textures/KawaiiEnvMapImpl.hpp>
#include "../MisakaRootImpl.hpp"

#include <qopengl.h>
#include <array>

class MISAKARENDERER_SHARED_EXPORT MisakaEnvMapImpl : public KawaiiEnvMapImpl
{
public:
  MisakaEnvMapImpl(KawaiiEnvMap *model);
  ~MisakaEnvMapImpl();

  // KawaiiEnvMapImpl interface
protected:
  inline void updateCache(DrawCache&, const QRect&) override final {}
  DrawCache *createDrawCache(const QRect &viewport) override final;

private:
  void attachTexture(KawaiiTextureImpl *tex, KawaiiEnvMap::AttachmentMode mode, int layer) override final;
  void attachDepthRenderbuffer(const QSize &sz) override final;
  void detachTexture(KawaiiEnvMap::AttachmentMode mode) override final;
  void deleteRenderbuffers() override final;
  void setRenderLayer(size_t i) override final;



  //IMPLEMENT
private:
  std::array<GLuint, 6> fboId;
  QSize depthRenderbufferSz;
  GLuint depthRenderbufId;

  bool hasColorAttachement;

  MisakaRootImpl *root();
};

#endif // MISAKAENVMAPIMPL_HPP
