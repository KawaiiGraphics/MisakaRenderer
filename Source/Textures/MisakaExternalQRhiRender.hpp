#ifndef MISAKAEXTERNALQRHIRENDER_HPP
#define MISAKAEXTERNALQRHIRENDER_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/Textures/KawaiiExternalQRhiRender.hpp>

#include "../MisakaRenderer_global.hpp"
#include "MisakaTextureHandle.hpp"

class MisakaRootImpl;

class MISAKARENDERER_SHARED_EXPORT MisakaExternalQRhiRender: public KawaiiRendererImpl
{
public:
  MisakaExternalQRhiRender(KawaiiExternalQRhiRender *model);
  ~MisakaExternalQRhiRender();



  //IMPLEMENT
private:
  MisakaRootImpl *root;
  MisakaTextureHandle *tex;

  QMetaObject::Connection onTexHandleChanged;

  std::unique_ptr<QRhiTexture> rhiTex;
  std::unique_ptr<QRhiRenderBuffer> rhiDepthStencil;

  void setTexture(KawaiiTexture *newTex);
  void updateRhiTex();

  void trySetRhi();

  void restoreGlState();

  void updateRoot();

  // KawaiiRendererImpl interface
private:
  void initConnections() override final;
};

#endif // MISAKAEXTERNALQRHIRENDER_HPP
