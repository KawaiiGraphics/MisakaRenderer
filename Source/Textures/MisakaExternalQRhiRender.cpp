#include "MisakaExternalQRhiRender.hpp"
#include "MisakaRootImpl.hpp"

#include <Kawaii3D/KawaiiRoot.hpp>
#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>
#include <QOpenGLFramebufferObject>

MisakaExternalQRhiRender::MisakaExternalQRhiRender(KawaiiExternalQRhiRender *model):
  KawaiiRendererImpl(model),
  root(nullptr),
  tex(nullptr)
{
  connect(model, &KawaiiExternalQRhiRender::renderTargetTextureChanged, this, &MisakaExternalQRhiRender::setTexture);
  connect(model, &KawaiiExternalQRhiRender::renderTargetTextureResized, this, &MisakaExternalQRhiRender::updateRhiTex);
  connect(model, &KawaiiExternalQRhiRender::rendered, this, &MisakaExternalQRhiRender::restoreGlState);
  connect(model, &sib_utils::TreeNode::parentUpdated, this, &MisakaExternalQRhiRender::updateRoot);
}

MisakaExternalQRhiRender::~MisakaExternalQRhiRender()
{
  if(getData())
    {
      auto rhiRenderTarget = static_cast<KawaiiExternalQRhiRender*>(getData())->getRenderTargetRhi();
      auto rhiRenderTargetRpDescr = rhiRenderTarget? rhiRenderTarget->renderPassDescriptor(): nullptr;

      if(rhiRenderTargetRpDescr)
        delete rhiRenderTargetRpDescr;
    }
}

void MisakaExternalQRhiRender::setTexture(KawaiiTexture *newTex)
{
  disconnect(onTexHandleChanged);

  auto newTexHandle = newTex && newTex->getRendererImpl()?
        static_cast<MisakaTextureHandle*>(
        static_cast<KawaiiTextureImpl*>(newTex->getRendererImpl())->getHandle())
      : nullptr;

  if(newTex && newTex->getRendererImpl())
    onTexHandleChanged = connect(static_cast<KawaiiTextureImpl*>(newTex->getRendererImpl()), &KawaiiTextureImpl::handleChanged,
                                 this, [this, newTex] { setTexture(newTex); });

  if(newTexHandle != tex)
    {
      tex = newTexHandle;
      updateRhiTex();
    }
}

void MisakaExternalQRhiRender::updateRhiTex()
{
  if(!tex) return;

  root->asyncTaskGL([this]{
      auto glTex = tex->getTexId();

      const auto sz = QSize(tex->getWidth(), tex->getHeight());
      if(!rhiTex || rhiTex->pixelSize() != sz
         || rhiTex->nativeTexture().object != static_cast<quint64>(glTex))
        {
          if(sz.width() && sz.height())
            {
              rhiTex.reset(root->getQRhi().newTexture(QRhiTexture::RGBA8, sz, 1, QRhiTexture::RenderTarget));
              bool ok = rhiTex->createFrom(QRhiTexture::NativeTexture {
                                             .object = static_cast<quint64>(glTex),
                                             .layout = 0 });
              if(ok)
                return trySetRhi();
              else
                qWarning().noquote() << QLatin1String("MisakaRenderer: WARNING: Could not create QRhiTexture!");
            }
          rhiTex.reset();
          rhiDepthStencil.reset();
        }
    });
}

void MisakaExternalQRhiRender::trySetRhi()
{
  if(root && rhiTex)
    {
      auto oldRhiRenderTarget = static_cast<KawaiiExternalQRhiRender*>(getData())->getRenderTargetRhi();
      auto oldRhiRenderTargetRpDescr = oldRhiRenderTarget? oldRhiRenderTarget->renderPassDescriptor(): nullptr;

      if(!rhiDepthStencil)
        {
          rhiDepthStencil.reset(root->getQRhi().newRenderBuffer(QRhiRenderBuffer::DepthStencil, rhiTex->pixelSize()));
          rhiDepthStencil->create();
        }
      if(rhiDepthStencil->pixelSize() != rhiTex->pixelSize())
        {
          rhiDepthStencil->setPixelSize(rhiTex->pixelSize());
          rhiDepthStencil->create();
        }

      auto rhiRenderTarget = root->getQRhi().newTextureRenderTarget({QRhiColorAttachment(rhiTex.get()), rhiDepthStencil.get()});
      rhiRenderTarget->setRenderPassDescriptor(rhiRenderTarget->newCompatibleRenderPassDescriptor());

      static_cast<KawaiiExternalQRhiRender*>(getData())->
          setRhi(&root->getQRhi(), rhiRenderTarget);

      if(oldRhiRenderTargetRpDescr)
        delete oldRhiRenderTargetRpDescr;
    }
}

void MisakaExternalQRhiRender::restoreGlState()
{
  if(Q_LIKELY(root))
    {
      root->taskGL([this]{
          root->finalize(rhiTex->pixelSize());
          root->rebindGlobalTexturesUbo();
        });
    }
}

void MisakaExternalQRhiRender::updateRoot()
{
  KawaiiRoot *kawaiiRoot = KawaiiRoot::getRoot(getData());
  auto newRoot = static_cast<MisakaRootImpl*>(kawaiiRoot->getRendererImpl());
  if(newRoot != root)
    {
      root = newRoot;
      trySetRhi();
    }
}

void MisakaExternalQRhiRender::initConnections()
{
  updateRoot();
  setTexture(static_cast<KawaiiExternalQRhiRender*>(getData())->getRenderTargetTexture());
}
