#include "MisakaFramebufferImpl.hpp"
#include "Renderpass/MisakaRenderpassImpl.hpp"
#include "MisakaRootImpl.hpp"
#include "Textures/MisakaTextureHandle.hpp"

MisakaFramebufferImpl::MisakaFramebufferImpl(KawaiiFramebuffer *model):
  KawaiiFramebufferImpl(model),
  fboId(0)
{
  if(root())
    root()->taskGL([this] {
        root()->gl().glCreateFramebuffers(1, &fboId);
      });
  connect(getModel(), &KawaiiFramebuffer::renderpassChanged, this, &MisakaFramebufferImpl::onRenderpassChanged);
}

MisakaFramebufferImpl::~MisakaFramebufferImpl()
{
  preDestruct();
}

void MisakaFramebufferImpl::attachTexture(KawaiiTextureImpl *tex, quint32 gbuf, int layer)
{
  static_cast<void>(tex);
  static_cast<void>(gbuf);
  static_cast<void>(layer);
}

void MisakaFramebufferImpl::detachTexture(quint32 gbuf)
{
  if(oldRp && oldRp->getRendererImpl())
    static_cast<MisakaRenderpassImpl*>(oldRp->getRendererImpl())->setExternalGbuf(gbuf, 0, -1);
}

void MisakaFramebufferImpl::activate()
{
  auto *rp = getModel()->getRenderpass();
  MisakaRenderpassImpl *rp_impl = rp? static_cast<MisakaRenderpassImpl*>(rp->getRendererImpl()): nullptr;
  getModel()->forallAtachments([rp_impl] (const KawaiiFramebuffer::Attachment &att) {
      auto *texture = static_cast<KawaiiTextureImpl*>(att.target->getRendererImpl());
      rp_impl->setExternalGbuf(att.useGbuf, static_cast<MisakaTextureHandle*>(texture->getHandle())->getTexId(), att.layer);
    });
}

void MisakaFramebufferImpl::deactivate()
{
}

void MisakaFramebufferImpl::destroy()
{
  root()->taskGL([this] {
      root()->gl().glDeleteFramebuffers(1, &fboId);
      fboId = 0;
    });
}

MisakaRootImpl *MisakaFramebufferImpl::root()
{
  return static_cast<MisakaRootImpl*>(getRoot());
}

void MisakaFramebufferImpl::onRenderpassChanged(KawaiiRenderpass *newRenderpass)
{
  if(oldRp == newRenderpass)
    return;

  if(oldRp && oldRp->getRendererImpl())
    static_cast<MisakaRenderpassImpl*>(oldRp->getRendererImpl())->unuseExternalGbufs();

  oldRp = newRenderpass;
}
