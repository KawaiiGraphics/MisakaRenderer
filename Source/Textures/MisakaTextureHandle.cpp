#include "MisakaTextureHandle.hpp"
#include "MisakaRootImpl.hpp"

namespace {
  GLenum trTextureType(KawaiiTextureType type)
  {
    switch (type)
      {
      case KawaiiTextureType::Texture1D:
        return GL_TEXTURE_1D;
      case KawaiiTextureType::Texture2D:
        return GL_TEXTURE_2D;
      case KawaiiTextureType::Texture3D:
        return GL_TEXTURE_3D;
      case KawaiiTextureType::TextureCube:
        return GL_TEXTURE_CUBE_MAP;
      case KawaiiTextureType::TextureBuffer:
        return GL_TEXTURE_BUFFER;
      case KawaiiTextureType::Texture1D_Array:
        return GL_TEXTURE_1D_ARRAY;
      case KawaiiTextureType::Texture2D_Array:
        return GL_TEXTURE_2D_ARRAY;
      case KawaiiTextureType::TextureRectangle:
        return GL_TEXTURE_RECTANGLE;
      case KawaiiTextureType::TextureCube_Array:
        return GL_TEXTURE_CUBE_MAP_ARRAY;
      case KawaiiTextureType::Texture2D_Multisample:
        return GL_TEXTURE_2D_MULTISAMPLE;
      case KawaiiTextureType::Texture2D_Multisample_Array:
        return GL_TEXTURE_2D_MULTISAMPLE_ARRAY;
      }
    Q_UNREACHABLE();
  }

  GLenum trTextureFilter(KawaiiTextureFilter filter)
  {
    switch(filter)
      {
      case KawaiiTextureFilter::Linear:
        return GL_LINEAR;
      case KawaiiTextureFilter::Nearest:
        return GL_NEAREST;
      case KawaiiTextureFilter::LinearMipmapLinear:
        return GL_LINEAR_MIPMAP_LINEAR;
      case KawaiiTextureFilter::LinearMipmapNearest:
        return GL_LINEAR_MIPMAP_NEAREST;
      case KawaiiTextureFilter::NearestMipmapLinear:
        return GL_NEAREST_MIPMAP_LINEAR;
      case KawaiiTextureFilter::NearestMipmapNearest:
        return GL_NEAREST_MIPMAP_NEAREST;
      }
    Q_UNREACHABLE();
  }

  GLenum trWrapMode(KawaiiTextureWrapMode mode)
  {
    switch(mode)
      {
      case KawaiiTextureWrapMode::Repeat:
        return GL_REPEAT;
      case KawaiiTextureWrapMode::ClampToEdge:
        return GL_CLAMP_TO_EDGE;
      case KawaiiTextureWrapMode::ClampToBorder:
        return GL_CLAMP_TO_BORDER;
      case KawaiiTextureWrapMode::MirroredRepeat:
        return GL_MIRRORED_REPEAT;
      case KawaiiTextureWrapMode::MirroredClampToEdge:
        return GL_MIRROR_CLAMP_TO_EDGE;
      }
    Q_UNREACHABLE();
  }

  GLenum trArgType(const uint8_t *)
  { return GL_UNSIGNED_BYTE; }

  GLenum trArgType(const float *)
  { return GL_FLOAT; }

  GLenum trTexFormat(KawaiiTextureFormat fmt)
  {
    switch(fmt)
      {
      case KawaiiTextureFormat::Red:
        return GL_RED;
      case KawaiiTextureFormat::RedGreen:
        return GL_RG;
      case KawaiiTextureFormat::RedGreenBlue:
        return GL_RGB;
      case KawaiiTextureFormat::RedGreenBlueAlpha:
        return GL_RGBA;
      case KawaiiTextureFormat::BlueGreenRed:
        return GL_BGR;
      case KawaiiTextureFormat::BlueGreenRedAlpha:
        return GL_BGRA;
      case KawaiiTextureFormat::Depth:
        return GL_DEPTH_COMPONENT;
      case KawaiiTextureFormat::DepthStencil:
        return GL_DEPTH_STENCIL;
      }
    Q_UNREACHABLE();
  }

  template<typename T>
  GLenum trInternalTexFormat(KawaiiTextureFormat fmt)
  {
    using T_ = sib_utils::refined_type<T>;
    if constexpr(std::is_same_v<T_, uint8_t>)
    {
      switch(fmt)
        {
        case KawaiiTextureFormat::Red:
          return GL_R8;
        case KawaiiTextureFormat::RedGreen:
          return GL_RG8;
        case KawaiiTextureFormat::RedGreenBlue:
        case KawaiiTextureFormat::BlueGreenRed:
          return GL_RGB8;
        case KawaiiTextureFormat::RedGreenBlueAlpha:
        case KawaiiTextureFormat::BlueGreenRedAlpha:
          return GL_RGBA8;
        case KawaiiTextureFormat::Depth:
          return GL_DEPTH_COMPONENT16;
        case KawaiiTextureFormat::DepthStencil:
          return GL_DEPTH24_STENCIL8;
        }
      Q_UNREACHABLE();
    } else if constexpr(std::is_same_v<T_, float>)
    {
      switch(fmt)
        {
        case KawaiiTextureFormat::Red:
          return GL_R32F;
        case KawaiiTextureFormat::RedGreen:
          return GL_RG32F;
        case KawaiiTextureFormat::RedGreenBlue:
        case KawaiiTextureFormat::BlueGreenRed:
          return GL_RGB32F;
        case KawaiiTextureFormat::RedGreenBlueAlpha:
        case KawaiiTextureFormat::BlueGreenRedAlpha:
          return GL_RGBA32F;
        case KawaiiTextureFormat::Depth:
          return GL_DEPTH_COMPONENT32;
        case KawaiiTextureFormat::DepthStencil:
          return GL_DEPTH24_STENCIL8;
        }
      Q_UNREACHABLE();
    } else
    {
      Q_UNREACHABLE();
    }
  }

  enum class SetTexDataMode
  {
    AllocatePersistent,
    Update
  };

  template<typename T>
  void setTex1D(QOpenGLFunctions_4_5_Core &gl, GLuint texId, int width, int levels, KawaiiTextureFormat fmt, const T *ptr, SetTexDataMode mode = SetTexDataMode::AllocatePersistent)
  {
    if(mode == SetTexDataMode::AllocatePersistent)
      gl.glTextureStorage1D(texId, levels, trInternalTexFormat<T>(fmt), width);
    if(ptr)
      {
        gl.glTextureSubImage1D(texId, 0, 0, width, trTexFormat(fmt), trArgType(ptr), ptr);
        if(levels > 1)
          gl.glGenerateTextureMipmap(texId);
      }
  }


  template<typename T>
  void setTex2D(QOpenGLFunctions_4_5_Core &gl, GLuint texId, int width, int height, int levels, KawaiiTextureFormat fmt, const T *ptr, SetTexDataMode mode = SetTexDataMode::AllocatePersistent)
  {
    if(mode == SetTexDataMode::AllocatePersistent)
      gl.glTextureStorage2D(texId, levels, trInternalTexFormat<T>(fmt), width, height);
    if(ptr)
      {
        gl.glTextureSubImage2D(texId, 0, 0,0, width, height, trTexFormat(fmt), trArgType(ptr), ptr);
        if(levels > 1)
          gl.glGenerateTextureMipmap(texId);
      }
  }

  template<typename T>
  void setTex3D(QOpenGLFunctions_4_5_Core &gl, GLuint texId, int width, int height, int depth, int levels, KawaiiTextureFormat fmt, const T *ptr, SetTexDataMode mode = SetTexDataMode::AllocatePersistent)
  {
    if(mode == SetTexDataMode::AllocatePersistent)
      gl.glTextureStorage3D(texId, levels, trInternalTexFormat<T>(fmt), width, height, depth);
    if(ptr)
      {
        gl.glTextureSubImage3D(texId, 0, 0,0,0, width, height, depth, trTexFormat(fmt), trArgType(ptr), ptr);
        if(levels > 1)
          gl.glGenerateTextureMipmap(texId);
      }
  }

  template<typename T>
  void setTexCube(QOpenGLFunctions_4_5_Core &gl, GLuint texId, int width, int height, int levels, KawaiiTextureFormat fmt, const T *ptr, SetTexDataMode mode = SetTexDataMode::AllocatePersistent)
  {
    if(mode == SetTexDataMode::AllocatePersistent)
      gl.glTextureStorage2D(texId, levels, trInternalTexFormat<T>(fmt), width, height);
    if(ptr)
      {
        gl.glTextureSubImage3D(texId, 0, 0,0,0, width, height, 6, trTexFormat(fmt), trArgType(ptr), ptr);
        if(levels > 1)
          gl.glGenerateTextureMipmap(texId);
      }
  }

  KawaiiTextureFilter filterNoMipmap(KawaiiTextureFilter filter)
  {
    switch(filter)
      {
      case KawaiiTextureFilter::LinearMipmapLinear:
      case KawaiiTextureFilter::LinearMipmapNearest:
        return KawaiiTextureFilter::Linear;

      case KawaiiTextureFilter::NearestMipmapLinear:
      case KawaiiTextureFilter::NearestMipmapNearest:
        return KawaiiTextureFilter::Nearest;

      default:
        return filter;
      }
  }
}

//todo: explicit mipmap

MisakaTextureHandle::MisakaTextureHandle(MisakaRootImpl &root_, KawaiiTexture *model):
  r(root_),
  handle(0),
  id(0),
  type(trTextureType(model->getType())),
  w(0),
  h(0),
  d(0),
  layers(0),
  internalFmt(0),
  storageType(StorageType::None),
  mipmap_needed(checkMipmapIsNeeded(model->getMinFilter())),
  handle_exists(false)
{
  r.taskGL([this, model] {
      r.gl().glCreateTextures(type, 1, &id);
      r.gl().glTextureParameterf(id, GL_TEXTURE_MAX_ANISOTROPY_EXT, r.getGlFloat(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT));
      r.gl().glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, trTextureFilter(model->getMinFilter()));
      r.gl().glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, trTextureFilter(filterNoMipmap(model->getMagFilter())));
      r.gl().glTextureParameteri(id, GL_TEXTURE_WRAP_S, trWrapMode(model->getWrapModeS()));
      r.gl().glTextureParameteri(id, GL_TEXTURE_WRAP_T, trWrapMode(model->getWrapModeT()));
      r.gl().glTextureParameteri(id, GL_TEXTURE_WRAP_R, trWrapMode(model->getWrapModeR()));
    });
}

MisakaTextureHandle::~MisakaTextureHandle()
{
  r.taskGL([this] {
      r.gl().glDeleteTextures(1, &id);
    });
}

GLuint64 MisakaTextureHandle::getTexHandle()
{
  if(!handle)
    {
      r.taskGL([this] {
          handle = r.ARB_bindless_texture().glGetTextureHandle(id);
          r.ARB_bindless_texture().glMakeTextureHandleResident(handle);
          handle_exists = true;
        });
    }
  return handle;
}

GLuint MisakaTextureHandle::getTexId() const
{
  return id;
}

void MisakaTextureHandle::attachToFbo(GLuint fboId, GLenum attachment, GLint layer, GLuint level)
{
  if(storageType == StorageType::None)
    {
      qWarning("MisakaRenderer warning: attempt to attach an empty texture to framebuffer");
      return;
    }

  if(layer < 0 || storageType == StorageType::Data2D)
    {
      if(Q_UNLIKELY(level > 0))
        qWarning("MisakaRenderer warning: attempt to attach a 2D texture as a layered texture");
      r.gl().glNamedFramebufferTexture(fboId, attachment, id, level);
    } else
    r.gl().glNamedFramebufferTextureLayer(fboId, attachment, id, level, layer);
}

GLenum MisakaTextureHandle::getInternalFmt() const
{
  return internalFmt;
}

int MisakaTextureHandle::getWidth() const
{
  return w;
}

int MisakaTextureHandle::getHeight() const
{
  return h;
}

void MisakaTextureHandle::setData1D(int width, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, fmt))
    return invalidate();

  r.asyncTaskGL([this, width, layers, fmt, ptr] {
      SetTexDataMode setDataMode = storageType == StorageType::None?
            SetTexDataMode::AllocatePersistent: SetTexDataMode::Update;

      if(layers < 0)
        {
          setTex1D(r.gl(), id, width, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
          storageType = StorageType::Data1D;
          h = 0;
        } else
        {
          setTex2D(r.gl(), id, width, layers, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
          storageType = StorageType::Data2D;
          h = layers;
        }
      storageFmt = fmt;
      internalFmt = trInternalTexFormat<decltype(*ptr)>(fmt);
      w = width;
      d = 0;
      this->layers = std::max<int>(layers, 1);
    });
}

void MisakaTextureHandle::setData1D(int width, int layers, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, fmt))
    return invalidate();

  r.asyncTaskGL([this, width, layers, fmt, ptr] {
      SetTexDataMode setDataMode = storageType == StorageType::None?
            SetTexDataMode::AllocatePersistent: SetTexDataMode::Update;

      if(layers < 0)
        {
          setTex1D(r.gl(), id, width, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
          storageType = StorageType::Data1D;
          h = 0;
        } else
        {
          setTex2D(r.gl(), id, width, layers, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
          storageType = StorageType::Data2D;
          h = layers;
        }
      storageFmt = fmt;
      internalFmt = trInternalTexFormat<decltype(*ptr)>(fmt);
      w = width;
      d = 0;
      this->layers = std::max<int>(layers, 1);
    });
}

void MisakaTextureHandle::setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, heigh, fmt))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, layers, fmt, ptr] {
      SetTexDataMode setDataMode = storageType == StorageType::None?
            SetTexDataMode::AllocatePersistent: SetTexDataMode::Update;

      if(layers < 0)
        {
          setTex2D(r.gl(), id, width, heigh, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
          d = 0;
          storageType = StorageType::Data2D;
        } else
        {
          setTex3D(r.gl(), id, width, heigh, layers, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
          d = layers;
          storageType = StorageType::Data3D;
        }
      storageFmt = fmt;
      internalFmt = trInternalTexFormat<decltype(*ptr)>(fmt);
      w = width;
      h = heigh;
      this->layers = std::max<int>(layers, 1);
    });
}

void MisakaTextureHandle::setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, heigh, fmt))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, layers, fmt, ptr] {
      SetTexDataMode setDataMode = storageType == StorageType::None?
            SetTexDataMode::AllocatePersistent: SetTexDataMode::Update;

      if(layers < 0)
        {
          setTex2D(r.gl(), id, width, heigh, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
          d = 0;
          storageType = StorageType::Data2D;
        } else
        {
          setTex3D(r.gl(), id, width, heigh, layers, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
          d = layers;
          storageType = StorageType::Data3D;
        }
      storageFmt = fmt;
      internalFmt = trInternalTexFormat<decltype(*ptr)>(fmt);
      w = width;
      h = heigh;
      this->layers = std::max<int>(layers, 1);
    });
}

void MisakaTextureHandle::setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, heigh, depth, fmt))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, depth, fmt, ptr] {
      SetTexDataMode setDataMode = storageType == StorageType::None?
            SetTexDataMode::AllocatePersistent: SetTexDataMode::Update;

      setTex3D(r.gl(), id, width, heigh, depth, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
      storageType = StorageType::Data3D;
      storageFmt = fmt;
      internalFmt = trInternalTexFormat<decltype(*ptr)>(fmt);
      w = width;
      h = heigh;
      d = depth;
      layers = 0;
    });
}

void MisakaTextureHandle::setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, heigh, depth, fmt))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, depth, fmt, ptr] {
      SetTexDataMode setDataMode = storageType == StorageType::None?
            SetTexDataMode::AllocatePersistent: SetTexDataMode::Update;

      setTex3D(r.gl(), id, width, heigh, depth, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
      storageType = StorageType::Data3D;
      storageFmt = fmt;
      internalFmt = trInternalTexFormat<decltype(*ptr)>(fmt);
      w = width;
      h = heigh;
      d = depth;
      layers = 0;
    });
}

void MisakaTextureHandle::setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, heigh, 6, fmt))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, layers, fmt, ptr] {
      SetTexDataMode setDataMode = storageType == StorageType::None?
            SetTexDataMode::AllocatePersistent: SetTexDataMode::Update;

      if(layers < 0)
        {
          setTexCube(r.gl(), id, width, heigh, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
          d = 6;
        } else
        {
          d = 6 * layers;
          setTex3D(r.gl(), id, width, heigh, d, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
        }
      storageType = StorageType::Data3D;
      storageFmt = fmt;
      internalFmt = trInternalTexFormat<decltype(*ptr)>(fmt);
      w = width;
      h = heigh;
      this->layers = d;
    });
}

void MisakaTextureHandle::setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr)
{
  const int layers_ = layers<0? 6: 6 * layers;

  if(!checkStorageCompatibility(width, heigh, layers_, fmt))
    return invalidate();

  r.asyncTaskGL([this, width, heigh, layers, layers_, fmt, ptr] {
      SetTexDataMode setDataMode = storageType == StorageType::None?
            SetTexDataMode::AllocatePersistent: SetTexDataMode::Update;

      if(layers < 0)
        {
          setTexCube(r.gl(), id, width, heigh, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
        } else
        {
          setTex3D(r.gl(), id, width, heigh, layers_, mipmap_needed? 4: 1, fmt, ptr, setDataMode);
        }
      storageType = StorageType::Data3D;
      storageFmt = fmt;
      internalFmt = trInternalTexFormat<decltype(*ptr)>(fmt);
      w = width;
      h = heigh;
      d = layers_;
      this->layers = layers_;
    });
}

void MisakaTextureHandle::setCompareOperation(KawaiiDepthCompareOperation op)
{
  switch(op)
    {
    case KawaiiDepthCompareOperation::None:
      r.asyncTaskGL([this] {
          r.gl().glTextureParameteri(id, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        });
      break;
    case KawaiiDepthCompareOperation::Less:
      r.asyncTaskGL([this] {
          r.gl().glTextureParameteri(id, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
          r.gl().glTextureParameteri(id, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
        });
      break;
    case KawaiiDepthCompareOperation::LessEqual:
      r.asyncTaskGL([this] {
          r.gl().glTextureParameteri(id, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
          r.gl().glTextureParameteri(id, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        });
      break;
    case KawaiiDepthCompareOperation::Greater:
      r.asyncTaskGL([this] {
          r.gl().glTextureParameteri(id, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
          r.gl().glTextureParameteri(id, GL_TEXTURE_COMPARE_FUNC, GL_GREATER);
        });
      break;
    case KawaiiDepthCompareOperation::GreaterEqual:
      r.asyncTaskGL([this] {
          r.gl().glTextureParameteri(id, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
          r.gl().glTextureParameteri(id, GL_TEXTURE_COMPARE_FUNC, GL_GEQUAL);
        });
      break;
    }
}

void MisakaTextureHandle::setMinFilter(KawaiiTextureFilter filter)
{
  //if handle exists, texture state becomes immutable, so the only way to chang tex parameters is recreate texture object
  if(handle_exists)
    return invalidate();

  bool mipmap = checkMipmapIsNeeded(filter);
  if(mipmap != mipmap_needed)
    {
      if(storageType == StorageType::None)
        mipmap_needed = mipmap;
      else
        return invalidate();
    }

  r.asyncTaskGL([this, filter] {
      r.gl().glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, trTextureFilter(filter));
    });
}

void MisakaTextureHandle::setMagFilter(KawaiiTextureFilter filter)
{
  //if handle exists, texture state becomes immutable, so the only way to chang tex parameters is recreate texture object
  if(handle_exists)
    return invalidate();

  r.asyncTaskGL([this, filter] {
      r.gl().glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, trTextureFilter(filterNoMipmap(filter)));
    });
}

void MisakaTextureHandle::setWrapModeS(KawaiiTextureWrapMode mode)
{
  //if handle exists, texture state becomes immutable, so the only way to chang tex parameters is recreate texture object
  if(handle_exists)
    return invalidate();

  r.asyncTaskGL([this, mode] {
      r.gl().glTextureParameteri(id, GL_TEXTURE_WRAP_S, trWrapMode(mode));
    });
}

void MisakaTextureHandle::setWrapModeT(KawaiiTextureWrapMode mode)
{
  //if handle exists, texture state becomes immutable, so the only way to chang tex parameters is recreate texture object
  if(handle_exists)
    return invalidate();

  r.asyncTaskGL([this, mode] {
      r.gl().glTextureParameteri(id, GL_TEXTURE_WRAP_T, trWrapMode(mode));
    });
}

void MisakaTextureHandle::setWrapModeR(KawaiiTextureWrapMode mode)
{
  //if handle exists, texture state becomes immutable, so the only way to chang tex parameters is recreate texture object
  if(handle_exists)
    return invalidate();

  r.asyncTaskGL([this, mode] {
      r.gl().glTextureParameteri(id, GL_TEXTURE_WRAP_R, trWrapMode(mode));
    });
}

bool MisakaTextureHandle::checkMipmapIsNeeded(KawaiiTextureFilter filter) const
{
  switch(filter)
    {
    case KawaiiTextureFilter::LinearMipmapLinear:
    case KawaiiTextureFilter::LinearMipmapNearest:
    case KawaiiTextureFilter::NearestMipmapLinear:
    case KawaiiTextureFilter::NearestMipmapNearest:
      return true;
      break;

    default:
      return false;
      break;
    }
}

bool MisakaTextureHandle::checkStorageCompatibility(int width, KawaiiTextureFormat fmt) const
{
  bool storageTypeMatches = (storageType == StorageType::Data1D);
  bool sizeMatches = (width == h);
  bool fmtMatches = (fmt == storageFmt);
  return (storageType == StorageType::None || (storageTypeMatches && sizeMatches && fmtMatches));
}

bool MisakaTextureHandle::checkStorageCompatibility(int width, int height, KawaiiTextureFormat fmt) const
{
  bool storageTypeMatches = (storageType == StorageType::Data2D);
  bool sizeMatches = (width == h && height == h);
  bool fmtMatches = (fmt == storageFmt);
  return (storageType == StorageType::None || (storageTypeMatches && sizeMatches && fmtMatches));
}

bool MisakaTextureHandle::checkStorageCompatibility(int width, int height, int depth, KawaiiTextureFormat fmt) const
{
  bool storageTypeMatches = (storageType == StorageType::Data3D);
  bool sizeMatches = (width == h && height == h && depth == d);
  bool fmtMatches = (fmt == storageFmt);
  return (storageType == StorageType::None || (storageTypeMatches && sizeMatches && fmtMatches));
}
