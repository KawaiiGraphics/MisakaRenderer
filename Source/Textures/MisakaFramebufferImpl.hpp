#ifndef MISAKAFRAMEBUFFERIMPL_HPP
#define MISAKAFRAMEBUFFERIMPL_HPP

#include <KawaiiRenderer/Textures/KawaiiFramebufferImpl.hpp>
#include "MisakaRenderer_global.hpp"
#include <qopengl.h>

class MisakaRootImpl;

class MISAKARENDERER_SHARED_EXPORT MisakaFramebufferImpl : public KawaiiFramebufferImpl
{
public:
  MisakaFramebufferImpl(KawaiiFramebuffer *model);
  ~MisakaFramebufferImpl();

  // KawaiiFramebufferImpl interface
private:
  void attachTexture(KawaiiTextureImpl *tex, quint32 gbuf, int layer) override final;
  void detachTexture(quint32 gbuf) override final;
  void activate() override final;
  void deactivate() override final;
  void destroy() override final;



  //IMPLEMENT
private:
  QPointer<KawaiiRenderpass> oldRp;

  GLuint fboId;

  MisakaRootImpl *root();

  void onRenderpassChanged(KawaiiRenderpass *newRenderpass);
};

#endif // MISAKAFRAMEBUFFERIMPL_HPP
