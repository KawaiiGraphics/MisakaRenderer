#ifndef MISAKATEXTUREHANDLE_HPP
#define MISAKATEXTUREHANDLE_HPP

#include <KawaiiRenderer/Textures/KawaiiTextureHandle.hpp>
#include "../MisakaRenderer_global.hpp"
#include <qopengl.h>

class MisakaRootImpl;

class MISAKARENDERER_SHARED_EXPORT MisakaTextureHandle : public KawaiiTextureHandle
{
public:
  MisakaTextureHandle(MisakaRootImpl &root, KawaiiTexture *model);
  ~MisakaTextureHandle();

  GLuint64 getTexHandle();
  GLuint getTexId() const;

  void attachToFbo(GLuint fboId, GLenum attachment, GLint layer, GLuint level = 0);

  GLenum getInternalFmt() const;

  int getWidth() const;
  int getHeight() const;

  // KawaiiTextureHandle interface
public:
  void setData1D(int width, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setData1D(int width, int layers, KawaiiTextureFormat fmt, const float *ptr) override final;
  void setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr) override final;
  void setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const float *ptr) override final;
  void setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr) override final;

  void setCompareOperation(KawaiiDepthCompareOperation op) override final;

  void setMinFilter(KawaiiTextureFilter filter) override final;
  void setMagFilter(KawaiiTextureFilter filter) override final;
  void setWrapModeS(KawaiiTextureWrapMode mode) override final;
  void setWrapModeT(KawaiiTextureWrapMode mode) override final;
  void setWrapModeR(KawaiiTextureWrapMode mode) override final;



  //IMPLEMENT
private:
  enum class StorageType: uint8_t {
    Data1D,
    Data2D,
    Data3D,
    None
  };

  MisakaRootImpl &r;

  GLuint64 handle;
  GLuint id;
  GLenum type;

  int w;
  int h;
  int d;
  int layers;

  GLenum internalFmt;
  KawaiiTextureFormat storageFmt;
  StorageType storageType;
  bool mipmap_needed;
  bool handle_exists;

  bool checkMipmapIsNeeded(KawaiiTextureFilter filter) const;

  bool checkStorageCompatibility(int width, KawaiiTextureFormat fmt) const;
  bool checkStorageCompatibility(int width, int height, KawaiiTextureFormat fmt) const;
  bool checkStorageCompatibility(int width, int height, int depth, KawaiiTextureFormat fmt) const;
};

#endif // MISAKATEXTUREHANDLE_HPP
