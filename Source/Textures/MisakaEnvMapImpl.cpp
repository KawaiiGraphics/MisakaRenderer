#include "MisakaEnvMapImpl.hpp"
#include "MisakaTextureHandle.hpp"

#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>

namespace {
  GLenum trAttachementMode(KawaiiEnvMap::AttachmentMode mode)
  {
    switch(mode)
      {
      case KawaiiEnvMap::AttachmentMode::Color:
        return GL_COLOR_ATTACHMENT0;
      case KawaiiEnvMap::AttachmentMode::Depth:
        return GL_DEPTH_ATTACHMENT;
      case KawaiiEnvMap::AttachmentMode::DepthStencil:
        return GL_DEPTH_STENCIL_ATTACHMENT;
      case KawaiiEnvMap::AttachmentMode::Stencil:
        return GL_STENCIL_ATTACHMENT;
      }
    Q_UNREACHABLE();
  }
}

MisakaEnvMapImpl::MisakaEnvMapImpl(KawaiiEnvMap *model):
  KawaiiEnvMapImpl(model),
  fboId({0,0,0,0,0,0}),
  depthRenderbufferSz(0,0),
  depthRenderbufId(0),
  hasColorAttachement(false)
{
  root()->taskGL([this] {
      root()->gl().glCreateFramebuffers(6, fboId.data());
    });
}

MisakaEnvMapImpl::~MisakaEnvMapImpl()
{
  preDestruct();
  root()->taskGL([this] {
      root()->gl().glDeleteFramebuffers(6, fboId.data());
    });
}

KawaiiEnvMapImpl::DrawCache *MisakaEnvMapImpl::createDrawCache(const QRect &viewport)
{
  root()->gl().glViewport(viewport.x(), viewport.y(), viewport.width(), viewport.height());
  drawScene();
  return nullptr;
}

void MisakaEnvMapImpl::attachTexture(KawaiiTextureImpl *tex, KawaiiEnvMap::AttachmentMode mode, int layer)
{
  auto texHandle = static_cast<MisakaTextureHandle*>(tex->getHandle());
  if(layer < 0)
    layer = 0;
  else
    layer *= 6;

  if(!hasColorAttachement && mode == KawaiiEnvMap::AttachmentMode::Color)
    hasColorAttachement = true;

  const GLenum attMode = trAttachementMode(mode);
  root()->taskGL([this, attMode, texHandle, layer] {
      if(texHandle)
        {
          for(size_t i = 0; i < 6; ++i)
            texHandle->attachToFbo(fboId[i], attMode, layer + i);
        } else
        {
          for(size_t i = 0; i < 6; ++i)
            root()->gl().glNamedFramebufferTexture(fboId[i], attMode, 0, 0);
        }
    });
}

void MisakaEnvMapImpl::attachDepthRenderbuffer(const QSize &sz)
{
  root()->taskGL([this, &sz] {
      if(!depthRenderbufId)
        root()->gl().glCreateRenderbuffers(1, &depthRenderbufId);

      if(depthRenderbufferSz != sz)
        {
          root()->gl().glNamedRenderbufferStorage(depthRenderbufId, GL_DEPTH_COMPONENT24, sz.width(), sz.height());
          depthRenderbufferSz = sz;
        }
      for(size_t i = 0; i < 6; ++i)
        root()->gl().glNamedFramebufferRenderbuffer(fboId[i], GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbufId);
    });
}

void MisakaEnvMapImpl::detachTexture(KawaiiEnvMap::AttachmentMode mode)
{
  if(hasColorAttachement && mode == KawaiiEnvMap::AttachmentMode::Color)
    hasColorAttachement = false;

  const GLenum attMode = trAttachementMode(mode);
  root()->taskGL([this, attMode] {
      for(size_t i = 0; i < 6; ++i)
        root()->gl().glNamedFramebufferTexture(fboId[i], attMode, 0, 0);
    });
}

void MisakaEnvMapImpl::deleteRenderbuffers()
{
  if(depthRenderbufId)
    {
      root()->gl().glDeleteRenderbuffers(1, &depthRenderbufId);
      depthRenderbufId = 0;
    }
}

namespace {
  const std::array<float, 4> clearColor = {0,0,0,0};
  const float clearDepth = 1.0;
}

void MisakaEnvMapImpl::setRenderLayer(size_t i)
{
  root()->gl().glBindFramebuffer(GL_FRAMEBUFFER, fboId[i]);
  if(hasColorAttachement)
    {
      root()->gl().glDrawBuffer(GL_COLOR_ATTACHMENT0);
      root()->gl().glClearBufferfv(GL_COLOR, 0, clearColor.data());
    } else
    root()->gl().glDrawBuffer(GL_NONE);

  root()->gl().glClearBufferfv(GL_DEPTH, 0, &clearDepth);
  root()->gl().glEnable(GL_DEPTH_TEST);
  root()->gl().glDepthMask(GL_TRUE);
  root()->gl().glDisable(GL_BLEND);
  root()->gl().glDepthFunc(GL_LESS);
}

MisakaRootImpl *MisakaEnvMapImpl::root()
{
  return static_cast<MisakaRootImpl*>(getRoot());
}
