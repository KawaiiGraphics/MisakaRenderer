#include "MisakaMeshImpl.hpp"
#include "MisakaRootImpl.hpp"

MisakaMeshImpl::MisakaMeshImpl(KawaiiMesh3D *model):
  KawaiiMeshImpl(model),
  vao_id(0)
{
}

MisakaMeshImpl::~MisakaMeshImpl()
{
  preDestruct();
}

void MisakaMeshImpl::draw() const
{
  root()->gl().glBindVertexArray(vao_id);
  root()->gl().glDrawRangeElements(GL_TRIANGLES, getMinIndex(), getMaxIndex(), 3 * getModel()->getRawTriangles().size(), GL_UNSIGNED_INT, nullptr);
}

void MisakaMeshImpl::drawInstanced(size_t instances) const
{
  root()->gl().glBindVertexArray(vao_id);
  root()->gl().glDrawElementsInstanced(GL_TRIANGLES, 3 * getModel()->getRawTriangles().size(), GL_UNSIGNED_INT, nullptr, instances);
}

void MisakaMeshImpl::createVertexArray()
{
  root()->taskGL([this] {
      root()->gl().glCreateVertexArrays(1, &vao_id);
      bindIndices();
      bindBaseAttr();
    });
}

void MisakaMeshImpl::deleteVertexArray()
{
  root()->taskGL([this] {
      root()->gl().glDeleteVertexArrays(1, &vao_id);
    });
}

void MisakaMeshImpl::bindBaseAttr()
{
  static constexpr GLuint posInd = 0, normInd = 1, texCInd = 2,
      globalOffset = 0;

  static_cast<MisakaBufferHandle*>(getBaseAttrBuf())->bindVbo(vao_id, 0, globalOffset, sizeof(KawaiiPoint3D));

  root()->gl().glEnableVertexArrayAttrib(vao_id, posInd);
  root()->gl().glVertexArrayAttribFormat(vao_id, posInd, 4, GL_FLOAT, false, 0);
  root()->gl().glVertexArrayAttribBinding(vao_id, posInd, 0);

  root()->gl().glEnableVertexArrayAttrib(vao_id, normInd);
  root()->gl().glVertexArrayAttribFormat(vao_id, normInd, 3, GL_FLOAT, false, sizeof(QVector4D));
  root()->gl().glVertexArrayAttribBinding(vao_id, normInd, 0);

  root()->gl().glEnableVertexArrayAttrib(vao_id, texCInd);
  root()->gl().glVertexArrayAttribFormat(vao_id, texCInd, 3, GL_FLOAT, false, sizeof(QVector4D)+sizeof(QVector3D));
  root()->gl().glVertexArrayAttribBinding(vao_id, texCInd, 0);
}

void MisakaMeshImpl::bindIndices()
{
  static_cast<MisakaBufferHandle*>(getIndicesBuffer())->bindElementBuffer(vao_id);
}

MisakaRootImpl *MisakaMeshImpl::root() const
{
  return static_cast<MisakaRootImpl*>(getRoot());
}
