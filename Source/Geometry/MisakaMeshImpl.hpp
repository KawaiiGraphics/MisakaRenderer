#ifndef MISAKAMESHIMPL_HPP
#define MISAKAMESHIMPL_HPP

#include <KawaiiRenderer/Geometry/KawaiiMeshImpl.hpp>
#include "../MisakaRenderer_global.hpp"
#include <qopengl.h>

class MisakaRootImpl;
class MISAKARENDERER_SHARED_EXPORT MisakaMeshImpl : public KawaiiMeshImpl
{
public:
  MisakaMeshImpl(KawaiiMesh3D *model);
  ~MisakaMeshImpl();

  // KawaiiMeshImpl interface
public:
  void draw() const override final;
  void drawInstanced(size_t instances) const override final;

private:
  void createVertexArray() override final;
  void deleteVertexArray() override final;
  void bindBaseAttr() override final;
  void bindIndices() override final;



  //IMPLEMENT
private:
  GLuint vao_id;

  MisakaRootImpl* root() const;
};

#endif // MISAKAMESHIMPL_HPP
