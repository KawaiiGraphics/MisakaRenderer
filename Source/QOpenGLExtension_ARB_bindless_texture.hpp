#ifndef QOPENGLEXTENSION_ARB_BINDLESS_TEXTURE_HPP
#define QOPENGLEXTENSION_ARB_BINDLESS_TEXTURE_HPP

#include <QOpenGLContext>

class QOpenGLExtension_ARB_bindless_texturePrivate
{
public:
  GLboolean (QOPENGLF_APIENTRYP isImageHandleResidentARB) (GLuint64 handle);
  GLboolean (QOPENGLF_APIENTRYP isTextureHandleResidentARB) (GLuint64 handle);
  void (QOPENGLF_APIENTRYP programUniformHandleui64vARB) (GLuint program, GLint location, GLsizei count, const GLuint64 *values);
  void (QOPENGLF_APIENTRYP programUniformHandleui64ARB) (GLuint program, GLint location, GLuint64 value);
  void (QOPENGLF_APIENTRYP uniformHandleui64vARB) (GLint location, GLsizei count, const GLuint64 *value);
  void (QOPENGLF_APIENTRYP uniformHandleui64ARB) (GLint location, GLuint64 value);
  void (QOPENGLF_APIENTRYP makeImageHandleNonResidentARB) (GLuint64 handle);
  void (QOPENGLF_APIENTRYP makeImageHandleResidentARB) (GLuint64 handle, GLenum access);
  GLuint64 (QOPENGLF_APIENTRYP getImageHandleARB) (GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format);
  void (QOPENGLF_APIENTRYP makeTextureHandleNonResidentARB) (GLuint64 handle);
  void (QOPENGLF_APIENTRYP makeTextureHandleResidentARB) (GLuint64 handle);
  GLuint64 (QOPENGLF_APIENTRYP getTextureSamplerHandleARB) (GLuint texture, GLuint sampler);
  GLuint64 (QOPENGLF_APIENTRYP getTextureHandleARB) (GLuint texture);
};

class QOpenGLExtension_ARB_bindless_texture
{
public:
  QOpenGLExtension_ARB_bindless_texture();
  ~QOpenGLExtension_ARB_bindless_texture();

  GLboolean glIsImageHandleResident(GLuint64 handle);
  GLboolean glIsTextureHandleResident(GLuint64 handle);
  void glProgramUniformHandleui64v(GLuint program, GLint location, GLsizei count, const GLuint64 *values);
  void glProgramUniformHandleui64(GLuint program, GLint location, GLuint64 value);
  void glUniformHandleui64v(GLint location, GLsizei count, const GLuint64 *value);
  void glUniformHandleui64(GLint location, GLuint64 value);
  void glMakeImageHandleNonResident(GLuint64 handle);
  void glMakeImageHandleResident(GLuint64 handle, GLenum access);
  GLuint64 glGetImageHandle(GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format);
  void glMakeTextureHandleNonResident(GLuint64 handle);
  void glMakeTextureHandleResident(GLuint64 handle);
  GLuint64 glGetTextureSamplerHandle(GLuint texture, GLuint sampler);
  GLuint64 glGetTextureHandle(GLuint texture);


  bool initializeOpenGLFunctions();

protected:
  QOpenGLExtension_ARB_bindless_texturePrivate *arbBindlessTexture;
};

#endif // QOPENGLEXTENSION_ARB_BINDLESS_TEXTURE_HPP
