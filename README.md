# MisakaRenderer
OpenGL (GLSL) render for Kawaii3D. Based on KawaiiRenderer.

The project aims to provide the best Kawaii3D experience (high performance, good robustness and full support for all of the Kawaii3D features) on modern desktop systems.

OpenGL 4.5 Core and ARB\_bindless\_texture are strictly required.

